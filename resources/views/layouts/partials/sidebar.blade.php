<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/img/avatar_plusis.jpg')}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    @if (Auth::user()->participant_id)
                        <p>{{ Auth::user()->participant->coordonnees->prenom }} {{ Auth::user()->participant->coordonnees->nom }}
                    @endif
                    @if (Auth::user()->employe_id)
                        <p>{{ Auth::user()->employe->coordonnees->prenom }} {{ Auth::user()->employe->coordonnees->nom }}
                    @endif
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">FUNCTIONS</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="treeview">
                <a href="#"><i class='fa fa-users'></i> <span>UTILISATEUR</span> <i class="fa fa-angle-left
                pull-right"></i></a>
                <ul class="treeview-menu">

                  {{--exemple pour utiliser @can --}}
                  @can('users.index')
                    <li><a href="{{ url('liste_utilisateur') }}">Liste d'utilisateur</a></li>
                  @endcan

                  @can('departement.show')
                  <li><a href="{{ url('departements') }}">Departements</a></li>
                  @endcan
                  @can('fonction.show')
                  <li><a href="{{ url('fonctions') }}">Fonctions</a></li>
                  @endcan
                  @can('participant.index')
                  <li><a href="{{ url('participants') }}">Liste des participants</a></li>
                  @else
                    @can('participant.show') 
                    <li><a href="{{ url('participants') }}/{{ Auth::user()->participant_id }}">Mon dossier participant</a></li>
                    @endcan
                  @endcan
                  @can('employe.show')
                  <li><a href="{{ url('employes') }}">Employés</a></li>
                  @endcan
                  @can('plan_travail.show')
                  <li><a href="{{ url('plan_travails') }}">Plan de travail</a></li>
                  @endcan
                  @can('plan_travail_element.show')
                  <li><a href="{{ url('plan_travail_elements') }}">Éléments plan de travail</a></li>
                  @endcan
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
