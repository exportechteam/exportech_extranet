<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <strong>Copyright &copy; 2018 <a href="http://exportechquebec.ca">exportechquebec.ca</a>.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="http://exportechquebec.ca">Exportech Québec</a>.
    </div>
    <!-- Default to the left -->

</footer>
