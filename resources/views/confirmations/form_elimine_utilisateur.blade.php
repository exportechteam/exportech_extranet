<div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border my-box-header">
              <h3 class="box-title">Suprimer Utilisateur</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class=" box-body">
            @if ($utilisateur->participant_id)
            <h3>Voulez-vous supprimer l'utilisateur {{ $utilisateur->participant->coordonnees->prenom }} {{ $utilisateur->participant->coordonnees->nom }} ?</h3>
            @endif
            @if ($utilisateur->employe_id)
            <h3>Voulez-vous supprimer l'utilisateur {{ $utilisateur->employe->coordonnees->prenom }} {{ $utilisateur->employe->coordonnees->nom }} ?</h3>
            @endif

            </div>

              <div class="box-footer">

              <form method="post" action="{{ url('supprimer_utilisateur') }}" id="f_supprimer_utilisateur" class="formentree" >

               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_utilisateur" value="{{ $utilisateur->id }}">

                <button type="button" class="btn btn-default" onclick="javascript:$('.div_modal').click();" >Quitter</button>
                <button type="submit" class="btn btn-danger" style="margin-left:20px;" >Supprimer utilisateur</button> </form>
              </div>


          </div>
          <!-- /.box -->





        </div>
