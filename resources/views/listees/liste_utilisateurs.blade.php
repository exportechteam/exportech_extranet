@extends('layouts.app')

@section('htmlheader_title')
	Home
@endsection


@section('main-content')


<section  id="contenido_principal">

	<div class="box box-primary box-gris">
		<div class="box-header">
			<h4 class="box-title">Nom utilisateur à chercher</h4>
	    <form   action="{{ url('chercher_utilisateur') }}"  method="post"  >
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<div class="input-group input-group-lg">
					<input type="text" class="form-control" id="donnee_cherchee" name="donnee_cherchee" required>
					<span class="input-group-btn">
						<input type="submit" class="btn btn-success" value="chercher" >
					</span>
				</div>
	    </form>

			<div class="margin" id="botones_control">
				<a href="{{ url("/liste_utilisateur") }}"  class="btn btn-sm btn-success" >Liste des Utilisateurs</a>
				@can('roles.create')
				<a href="javascript:void(0);" class="btn btn-sm btn-success" onclick="chager_formulaire(2);">Rôle</a>
				<a href="javascript:void(0);" class="btn btn-sm btn-success" onclick="chager_formulaire(3);" >Permis</a>
				@endcan
			</div>
	  </div>


			<div class="box-body box-white">

		    <div class="table-responsive" >
			    <table  class="table table-hover table-striped" cellspacing="0" width="100%">
						<thead>
								<tr style="color:white; text-align: center; background-color: #016742;">
									  <th>code</th>
										<th>Rôle</th>
										<th>Nom</th>
										<th>Courriel</th>
									  <th>Action</th>
								</tr>
						</thead>
				    <tbody>
					    @foreach($utilisateurs as $utilisateur)
						  <tr role="row" class="odd">
							  <td>{{ $utilisateur->id }}</td>
							  <td>
									<span class="label label-default">

				             @foreach($utilisateur->getRoles() as $role)
							      {{  $role.","  }}
				             @endforeach

				          </span>
						 		</td>
								@if ($utilisateur->participant_id)
								<td class="mailbox-messages mailbox-name"><a href="javascript:void(0);"  style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;    {{ $utilisateur->participant->coordonnees->prenom }} {{ $utilisateur->participant->coordonnees->nom }} </a></td>
								@endif
								@if ($utilisateur->employe_id)
								<td class="mailbox-messages mailbox-name"><a href="javascript:void(0);"  style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;   {{ $utilisateur->employe->coordonnees->prenom }} {{ $utilisateur->employe->coordonnees->nom }}</span> </a></td>
								@endif
								<td>{{ $utilisateur->email }}</td>
								<td>
									<button type="button" class="btn  btn-default btn-xs" onclick="voirinfo_utilisateur({{  $utilisateur->id }})" ><i class="fa fa-fw fa-edit"></i></button>
									<button type="button"  class="btn  btn-danger btn-xs"  onclick="eliminer_utilisateur({{  $utilisateur->id }});"  ><i class="fa fa-fw fa-remove"></i></button>
								</td>
							</tr>
					    @endforeach
						</tbody>
					</table>
				</div>
			</div>




			{{ $utilisateurs->links() }}

			@if(count($utilisateurs)==0)


		<div class="box box-primary col-xs-12">

			<div class='aprobado' style="margin-top:70px; text-align: center">

				<label style='color:#177F6B'>
				              ... ils n'ont pas trouvé résultés pour sa recherche...
				</label>

			</div>

	 	</div>


		@endif

	</div>
</section>
@endsection
