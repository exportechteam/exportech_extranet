@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Ajouter un département</h1>
            {!! Form::open(['route' => 'departements.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('nom_departement', 'Nom')}}
                    {{Form::text('nom_departement', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}
                    {{Form::label('nombre_semaine_integration', 'Nombre de semaine intégration')}}
                    {{Form::text('nombre_semaine_integration', '', ['class' => 'form-control', 'placeholder' => 'Nombre de semaine'])}}
                    {{Form::label('max_semaine_prolongation', 'Maximum de semaine de prolongation')}}
                    {{Form::text('max_semaine_prolongation', '', ['class' => 'form-control', 'placeholder' => 'Nombre de semaine'])}}
                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">

        </script>
@endsection
