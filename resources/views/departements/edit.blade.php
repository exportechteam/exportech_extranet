@extends('layouts.app')

@section('main-content')

        <div class="container">
            @include('flash::message')

            <h1>Modifier un département</h1>



            {!! Form::open(['route' => ['departements.update', $departements->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                <div class="form-group">
                    {{Form::label('nom_departement', 'Nom')}}
                    {{Form::text('nom_departement', $departements->nom_departement, ['class' => 'form-control', 'placeholder' => 'Nom'])}}
                    {{Form::label('nombre_semaine_integration', 'Nombre de semaine intégration')}}
                    {{Form::text('nombre_semaine_integration', $departements->nombre_semaine_integration, ['class' => 'form-control', 'placeholder' => 'Nombre de semaine'])}}
                    {{Form::label('max_semaine_prolongation', 'Maximum de semaine de prolongation')}}
                    {{Form::text('max_semaine_prolongation', $departements->max_semaine_prolongation, ['class' => 'form-control', 'placeholder' => 'Nombre de semaine'])}}
                     {!! Form::hidden('_method','PUT') !!}  
                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
            <a href="/">Annuler</a>
        </div>

@endsection
