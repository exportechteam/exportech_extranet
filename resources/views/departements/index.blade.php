@extends('layouts.app')

@section('main-content')
      <div class="container">
        @include('flash::message')
        <a href="departements/create" class="btn btn-success">Créer un département</a>
        <div class="row">
          <div class="col-sm-12">
            @foreach($departements as $departement)
            <div class="card">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-block">
                    <p>
                      Créé le: {{$departement->created_at}}
                    </p>
                    <h4 class="card-title">{{$departement->nom_departement}}</h4>
                    <a href="departements/{{$departement->id}}/edit" class="btn btn-success">Modifier le département</a>
                  
                  </div>
                </div>

              </div>
            </div>
            @endforeach
            </div>

          </div>

      </div>
@endsection
