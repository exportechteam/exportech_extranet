
<section >


<div class="row" >

<div class="col-md-12">

  <div class="box box-primary box-gris">
    <div class="box-header with-border my-box-header">
        <h3 class="box-title"style="color:white; text-align: center;"><strong>Assigner rôle</strong></h3>
        <br>
        <h3 class="box-title"style="color:white; text-align: center;"><strong>Modifier les droits de l'utilisateur : </strong></h3> 
        @if ($utilisateur->participant_id)
            <h3 class="box-title"><strong>{{ $utilisateur->participant->coordonnees->prenom }}  {{ $utilisateur->participant->coordonnees->nom }}</strong></h3> 
        @endif
        @if ($utilisateur->employe_id)
            <h3 class="box-title"><strong>{{ $utilisateur->employe->coordonnees->prenom }}  {{ $utilisateur->employe->coordonnees->nom }}</strong></h3> 
        @endif   
    </div><!-- /.box-header -->
    Rôles assignés:
    <div id="zone_etiquettes_role" style="background-color:white;" >

    @foreach($utilisateur->getRoles() as $rl)
      <span class="label label-warning" style="margin-left:10px;">{{ $rl }} </span>
    @endforeach

  </div>
    <div class="box-body">

          <div class="col-md-12">
            <br>
            <div class="form-group">
            <label class="col-sm-2" for="tipo">Le rôle à assigner*</label>
                <div class="col-sm-6" >
                  <select id="rol1" name="rol1" class="form-control">

                           @foreach($role as $rol)
                           <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                           @endforeach
                  </select>
                </div>

                <div class="col-sm-4" >

                  <button type="button" class="btn btn-sm btn-success" onclick="assigner_role({{ $utilisateur->id }});" >Assigner rôle</button>
                </div>


            </div>

          </div>
          <hr>

           <div class="col-md-12">
            <div class="form-group">
            <label class="col-sm-2" for="tipo">Le rôle à retirer*</label>
                <div class="col-sm-6" >
                  <select id="rol2" name="rol2" class="form-control">
                           @foreach($role as $rol)
                           <option value="{{ $rol->id }}">{{ $rol->name }}</option>
                           @endforeach
                  </select>
                </div>

                <div class="col-sm-4" >
                  <button type="button" class="btn btn-sm btn-success" onclick="eliminer_role({{ $utilisateur->id }});" >Retirer rôle</button>
                </div>


            </div>

          </div>
    </div>

  </div> <!--box -->


  <div class="box box-primary   box-gris" style="margin-bottom: 200px;">
    <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Accès au système</strong></h3>
    </div><!-- /.box-header -->
    <div id="notification_E3" ></div>
    <div class="box-body">


                  <div class="box-header with-border my-box-header col-md-12" style="margin-bottom:15px;margin-top: 15px;">
                    <h3 class="box-title"><strong>Données d'accès</strong></h3>
                  </div>


                <form   action="{{ url('editer_acces') }}"  method="post" id="f_editer_acces"  class="formentree"  >
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_utilisateur" value="{{ $utilisateur->id }}">

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-2" for="email">Courriel*</label>
                    <div class="col-sm-10" >
                    <input type="email" class="form-control" id="email" name="email"  value="{{ $utilisateur->email  }}"  required >
                    </div>

                    </div><!-- /.form-group -->

                  </div><!-- /.col -->

                  <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-2" for="email">Nouveau mot de passe*</label>
                    <div class="col-sm-10" >
                    <input type="password" class="form-control" id="password" name="password"  required >
                    </div>

                    </div><!-- /.form-group -->

                  </div><!-- /.col -->


                    <div class=" col-xs-12 box-gris ">
                        <button type="submit" class="btn btn-success">Actualiser l'Accèss</button>
                    </div>

                   </form>

         </div>

  </div>
  </div>
</div>
</section>
