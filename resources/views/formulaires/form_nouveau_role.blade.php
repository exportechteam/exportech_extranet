{{--@can('roles.create') --}}
<section  >
<div class="col-md-12">

    <div class="box box-primary  box-gris">

            <div class="box-header with-border my-box-header">
                <h3 class="box-title"><strong>Nouveau Rôle</strong></h3>
            </div><!-- /.box-header -->

            {{-- <hr style="border-color:white;" /> --}}

            <div class="box-body"><br>


            <form   action="{{ url('creer_role') }}"  method="post" id="f_creer_role" class="formentree"  >
				          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <div class="col-md-6">
	                <div class="form-group">
							      <label class="col-sm-2" for="rol_nom">Nom du Rôle*</label>
	                  <div class="col-sm-10" >
							        <input type="text" class="form-control" id="rol_nom" name="rol_nom"  required >
	                  </div>
					  </div><!-- /.form-group -->

			    </div><!-- /.col -->

			      <div class="col-md-6">
	                <div class="form-group">
							<label class="col-sm-2" for="rol_slug">Slug*</label>
	                    <div class="col-sm-10" >
							<input type="text" class="form-control" id="rol_slug" name="rol_slug"  required >
	                    </div>
					</div><!-- /.form-group -->

			    </div><!-- /.col -->

			      <div class="col-md-6">
	                <div class="form-group">
							<label class="col-sm-2" for="rol_description">Description*</label>
	                    <div class="col-sm-10" >
							<input type="text" class="form-control" id="rol_description" name="rol_description"  required >
            </div><br>
					</div><!-- /.form-group -->

        </div><!-- /.col -->


                <div class="box-footer col-xs-12 box-gris ">
                  <br>
                        <button type="submit" class="btn btn-success">Créer Nouveau Rôle</button>
                </div>
            </form>

            </div>

    </div>

</div>


<div class="col-md-12">

    <div class="table-responsive" >

	    <table  class="table table-hover table-striped" cellspacing="0" width="100%">
				<thead>
						<tr style="color:white; text-align: center; background-color: #016742;">
                <th>code</th>
								<th>nom</th>
								<th>slug</th>
								<th>description</th>
							  <th>action</th>
						</tr>
				</thead>
	    <tbody>

	    @foreach($role as $rol)
		<tr role="row" class="odd" id="filaR_{{  $rol->id }}">
			<td>{{ $rol->id }}</td>
			<td><span class="label label-default">{{ $rol->name or "Aucun" }}</span></td>
			<td class="mailbox-messages mailbox-name"><a href="javascript:void(0);" style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;{{ $rol->slug  }}</a></td>
			<td>{{ $rol->description }}</td>
			<td>
			<button type="button"  class="btn  btn-danger btn-xs" onclick="borrar_rol({{ $rol->id }});"   ><i class="fa fa-fw fa-remove"></i></button>
			</td>
		</tr>
	    @endforeach



		</tbody>
		</table>

	</div>
</div>


</section>
{{-- @else
	<br/><div class='rechazado'><label style='color:#FA206A'>Vous n'avez pas de permission dans cette section</label>
	</div>
@endcan--}}
