{{--@role('administrateur') --}}

	<div class="col-md-12">

		<div class="box box-primary col-md-12 box-gris">

			<div class="box-header with-border my-box-header">
			<h3 class="box-title"><strong>Nouveau permis</strong></h3>
			</div><!-- /.box-header -->


			<div class="box-body">

					<div class="col-md-5">
						<br>
						 <form   action="{{ url('assigner_permission') }}"  method="post" id="f_assigner_permission" class="formentree"  >
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

							<div class="form-group">
								<label class="col-sm-2" for="rol">Rôle*</label>
							<div class="col-sm-10" >

								 <select id="rol_sel" name="rol_sel" class="form-control" required>
								 @foreach($role as $rol)
								 <option value="{{ $rol->id }}">{{ $rol->name }}</option>
								 @endforeach
								</select>

							</div>
							</div><!-- /.form-group -->


							<div class="form-group">
								<label class="col-sm-2" for="rol">Permis*</label>
							<div class="col-sm-10" >

								 <select id="permis_rol" name="permis_rol" class="form-control" required>
								 @foreach($permis as $perm)
								 <option value="{{ $perm->id }}">{{ $perm->name }}</option>
								 @endforeach
								</select>

							</div>
							</div><!-- /.form-group -->

							<div class="box-footer col-xs-12 box-gris ">
									<button type="submit" class="btn btn-success">Ajoute Permis</button>
							</div>
						 </form>
					</div>
					<div class="col-md-1"></div>



					<div class="col-md-6">

						<form   action="{{ url('creer_permission') }}"  method="post" id="f_creer_permission" class="formentree"  >
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">




							<div class="col-md-12">
								<br>
								<div class="form-group">
										<label class="col-sm-2" for="permis_nom">Permis*</label>
									<div class="col-sm-10" >
										<input type="text" class="form-control" id="permis_nom" name="permis_nom" required >
									</div>
								</div><!-- /.form-group -->

							</div><!-- /.col -->

							  <div class="col-md-12">
								<div class="form-group">
										<label class="col-sm-2" for="permis_slug">Slug*</label>
									<div class="col-sm-10" >
										<input type="text" class="form-control" id="permis_slug" name="permis_slug"  required >
									</div>
								</div><!-- /.form-group -->

							</div><!-- /.col -->

							  <div class="col-md-12">
								<div class="form-group">
										<label class="col-sm-2" for="apellido">Description*</label>
									<div class="col-sm-10" >
										<input type="text" class="form-control" id="permis_description" name="permis_description"  required >
									</div>
								</div><!-- /.form-group -->

							</div><!-- /.col -->


							<div class="box-footer col-xs-12 box-gris ">
									<button type="submit" class="btn btn-success">Créer Nouveau Permis</button>
							</div>
						</form>
					</div>
			</div>

		</div>

	</div>


	<div class="col-md-12 box-white">

	@foreach($role as $rol)

		<div class="table-responsive" >

			<table  class="table table-hover table-striped" cellspacing="0" width="100%">

					<thead>
					<th colspan="5" style="color:white; text-align: center; background-color: #016742;" >Permis d'Utilisateur {{ $rol->name }}</th>
					</thead>
					<thead>
								<th>code</th>
									<th>nom</th>
									<th>slug</th>
									<th>description</th>
									<th>Action</th>

					</thead>
			<tbody>


			@foreach($rol->permissions as $permi)


			 <tr role="row" class="odd" id="filaP_{{ $permi->id }}">
				<td>{{ $permi->id }}</td>
				<td><span class="label label-default">{{ $permi->name or "Aucun" }}</span></td>
				<td class="mailbox-messages mailbox-name"><a href="javascript:void(0);" style="display:block"></i>&nbsp;&nbsp;{{ $permi->slug  }}</a></td>
				<td>{{ $permi->description }}</td>
				<td>
				<button type="button"  class="btn  btn-danger btn-xs"  onclick="eliminer_permis({{ $rol->id }},{{ $permi->id }});"  ><i class="fa fa-fw fa-remove"></i></button>
				</td>
			   </tr>

			@endforeach
			</tbody>
			</table>

		</div>
	@endforeach

	</div>
{{--@else
	<br/><div class='rechazado'><label style='color:#FA206A'>Vous n'avez pas de permission dans cette section</label>
	</div>
@endrole--}}
