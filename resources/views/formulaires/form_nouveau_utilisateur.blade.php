<section class="content" >

 <div class="col-md-12">

  <div class="box box-primary  box-gris">
    <div class="box-header with-border my-box-header">
      <h3 class="box-title"><strong>Nouveau Utilisateur</strong></h3>
    </div><!-- /.box-header -->
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      {{--<hr style="border-color:white;" /> --}}

    <div class="box-body">

      <form   action="{{ url('creer_utilisateur') }}"  method="post" id="f_creer_utilisateur" class="formentree" >
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

          <div class="col-md-6">
            <br>
                  <div class="form-group">
                    <label class="col-sm-2" for="prenom">Prénom*</label>
                    <div class="col-sm-10" >
                      <input type="text" class="form-control" id="prenom" name="prenom"  required   >
                       </div>
                  </div><!-- /.form-group -->



          </div><!-- /.col -->


        <div class="col-md-6">
          <br>
                  <div class="form-group">
									  <label class="col-sm-2" for="nom">Nom*</label>
                    <div class="col-sm-10" >
										<input type="text" class="form-control" id="nom" name="nom"  required >
                    </div>
									</div><!-- /.form-group -->

				</div><!-- /.col -->

        <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-2" for="telephone">Téléphone*</label>

                       <div class="col-sm-10" >
                        <input type="text" class="form-control" id="telephone" name="telephone" required >
                       </div>

                      </div><!-- /.form-group -->

        </div><!-- /.col -->


        <div class="box-header with-border my-box-header col-md-12" style="margin-bottom:15px;margin-top: 15px;">
                    <h3 class="box-title"><strong>Données d'accèss</strong></h3>
        </div>


                <div class="col-md-6">
                  <br>
                  <div class="form-group">
                    <label class="col-sm-2" for="email">Courriel*</label>
                    <div class="col-sm-10" >
                    <input type="email" class="form-control" id="email" name="email"  required >
                    </div>

                    </div><!-- /.form-group -->

                  </div><!-- /.col -->

                  <div class="col-md-6">
                    <br>
                  <div class="form-group">
                    <label class="col-sm-2" for="email">Mot de passe*</label>
                    <div class="col-sm-10" >
                    <input type="password" class="form-control" id="password" name="password"  required >
                    </div>

                    </div><!-- /.form-group -->

                  </div><!-- /.col -->






                    <div class="box-footer col-xs-12 box-gris ">
                      <br>

                            <button type="submit" class="btn btn-success">Créer Nouveau Utilisateur</button>
                    </div>


                   </form>

                    </div>

                    </div>

                    </div>
</section>
