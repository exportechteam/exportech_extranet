@extends('layouts.app')

@section('main-content')

        <div class="container">
            @include('flash::message')

            <h1>Modifier une fonction</h1>



            {!! Form::open(['route' => ['fonctions.update', $fonctions->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                <div class="form-group">
                    {{Form::label('nom_fonction', 'Nom')}}
                    {{Form::text('nom_fonction', $fonctions->nom_fonction, ['class' => 'form-control', 'placeholder' => 'Nom'])}}

                    {!! Form::hidden('_method','PUT') !!}
                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
            <a href="/">Annuler</a>
        </div>

@endsection
