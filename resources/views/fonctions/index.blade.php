@extends('layouts.app')

@section('main-content')
      <div class="container">
        @include('flash::message')
        <a href="fonctions/create" class="btn btn-success">Créer une fonction</a>
        <div class="row">
          <div class="col-sm-12">
            @foreach($fonctions as $fonction)
            <div class="card">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-block">
                    <p>
                      Créé le: {{$fonction->created_at}}
                    </p>
                    <h4 class="card-title">{{$fonction->nom_fonction}}</h4>
                    <a href="fonctions/{{$fonction->id}}/edit" class="btn btn-success">Modifier la fonction</a>

                  </div>
                </div>

              </div>
            </div>
            @endforeach
            </div>

          </div>

      </div>
@endsection
