@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Ajouter une fonction</h1>
            {!! Form::open(['route' => 'fonctions.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('nom_fonction', 'Nom')}}
                    {{Form::text('nom_fonction', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}

                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">

        </script>
@endsection
