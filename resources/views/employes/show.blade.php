
@extends('layouts.app')

@section('main-content')

      <div class="topOff container bottomOff">
        <!--pour admin et coordonateur-->
        <a href="{{$employes->id}}/edit" class="btn btn-success">Modifier</a>
        <div class="row">
          <div class="col-md-12">
            @foreach($coordonnees as $coor)
            @if($coor->id == $employes->coordonnees_id)
            <h1>{{$coor->nom}}, {{$coor->prenom}}</h1>
            @endif
            @endforeach
            @foreach($departements as $dep)
              @if($dep->id == $employes->departement_id)
                <p>
                  Département: {{$dep->nom_departement}}
                </p>
              @endif
            @endforeach

            @foreach($fonctions as $fonc)
              @if($fonc->id == $employes->fonction_id)
                <p>
                  Fonction: {{$fonc->nom_fonction}}
                </p>
              @endif
            @endforeach

            @foreach($coordonnees as $coor)
              @if($coor->id == $employes->coordonnees_id)
            <p>
              Sexe: {{$coor->sexe}}
            </p>
            <p>
              Lieu de naissance: {{$coor->lieu_naissance}}
            </p>
            <p>
              Date de naissance: {{$coor->date_naissance}}
            </p>

                <p>
                  Adresse: {{$coor->adresse}}
                </p>
                <p>
                  Appartement: {{$coor->appartement}}
                </p>
                <p>
                  Ville: {{$coor->ville}}
                </p>
                <p>
                  Province: {{$coor->province}}
                </p>
                <p>
                  Code postal: {{$coor->code_postal}}
                </p>
                <p>
                  Téléphone: {{$coor->telephone}}
                </p>
                <p>
                  Créé: {{$coor->created_at}}
                </p>
              @endif
            @endforeach

            <a href="/" class="btn btn-info">Retour à la page principale</a>

          </div>
        </div>

      </div>
@endsection
