@extends('layouts.app')

@section('main-content')


        <div class="container">

          <?php
            $departementPresentement = $employes->departement_id;
            $fonctionPresentement = $employes->fonction_id;
           ?>
            @include('flash::message')

            <h1>Modifier les données d'un employé</h1>

            {!! Form::open(['route' => ['employes.update', $employes->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  @foreach($coordonnees as $coor)
                  @if($coor->id == $employes->coordonnees_id)
                    {{Form::label('prenom', 'Prénom')}}
                    {{Form::text('prenom', $coor->prenom, ['class' => 'form-control', 'placeholder' => 'Prénom'])}}

                    {{Form::label('nom', 'Nom')}}
                    {{Form::text('nom', $coor->nom, ['class' => 'form-control', 'placeholder' => 'Nom'])}}

                    {{Form::label('sexe', 'Sexe')}}
                    {{Form::select('sexe', array('homme' => 'Homme', 'femme' => 'Femme', 'nonbinaire' => 'Non-binaire'),$coor->sexe, ['class' => 'form-control'])}}

                    {{Form::label('lieu_naissance', 'Lieu de naissance')}}
                    {{Form::text('lieu_naissance', $coor->lieu_naissance, ['class' => 'form-control', 'placeholder' => 'Lieu de naissance'])}}

                    {{Form::label('date_naissance', 'Date de naissance')}}
                    {{Form::date('date_naissance', $coor->date_naissance, ['class' => 'form-control', 'placeholder' => 'AAAA-MM-JJ'])}}

                    @foreach($coordonnees as $coor)
                    @if($coor->id == $employes->coordonnees_id)
                    {{Form::label('telephone', 'Téléphone')}}
                    {{Form::text('telephone', $coor->telephone, ['class' => 'form-control', 'placeholder' => '1231231234'])}}
                    @endif
                    @endforeach


                    {{Form::label('departement', 'Departement')}}
                    {{Form::select('departement', $select, $departementPresentement, ['class' => 'form-control'])}}

                    {{Form::label('fonction', 'Fonction')}}
                    {{Form::select('fonction', $selectdeux,$fonctionPresentement, ['class' => 'form-control'])}}
                    @endif
                    @endforeach
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  @foreach($coordonnees as $coor)
                  @if($coor->id == $employes->coordonnees_id)
                    {{Form::label('adresse', 'Adresse')}}
                    {{Form::text('adresse', $coor->adresse, ['class' => 'form-control', 'placeholder' => 'Adresse'])}}

                    {{Form::label('app', 'Appartement')}}
                    {{Form::text('app', $coor->appartement, ['class' => 'form-control', 'placeholder' => 'Appartement'])}}

                    {{Form::label('ville', 'Ville')}}
                    {{Form::text('ville', $coor->ville, ['class' => 'form-control', 'placeholder' => 'Ville'])}}

                    {{Form::label('province', 'Province')}}
                    {{Form::text('province', $coor->province, ['class' => 'form-control', 'placeholder' => 'Province'])}}

                    {{Form::label('code_postal', 'Code postal')}}
                    {{Form::text('code_postal', $coor->code_postal, ['class' => 'form-control', 'placeholder' => 'g0g1h1'])}}
                    @endif
                    @endforeach
                </div>
            </div>
          </div>
            {!! Form::submit('Soumettre') !!}
            {!! Form::close() !!}
            @include('inc.messages')
            <a href="/">Annuler</a>
        </div>
@endsection
