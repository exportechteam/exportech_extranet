<div class="container topOff">
  @include('flash::message')
  <a href="employes/create" class="btn btn-success">Créer un employé</a>
  <div class="row">
    <div class="col-sm-12">
      @foreach($employes as $employe)
      <div class="card">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-block">
              <p>
                Créé le: {{$employe->created_at}}
              </p>
              @foreach($coordonnes as $coor)
              @if($coor->id == $employe->coordonnees_id)
              <h4 class="card-title">{{$coor->prenom}}, {{$coor->nom}}</h4>
              @endif
              @endforeach
              @foreach($departements as $dep)
              @if($dep->id == $employe->departement_id)
                <p>
                  {{ $dep->nom_departement }}
                </p>
              @endif
              @endforeach

              @foreach($fonctions as $fonc)
              @if($fonc->id == $employe->fonction_id)
                <p>
                  {{ $fonc->nom_fonction }}
                </p>
              @endif
              @endforeach

              <a href="employes/{{$employe->id}}" class="btn btn-info">Consulter</a>

            </div>
          </div>

        </div>
      </div>
      @endforeach
      </div>


    </div>

</div>
