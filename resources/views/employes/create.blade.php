@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Ajouter un Employé</h1>
            {!! Form::open(['route' => 'employes.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                    {{Form::label('prenom', 'Prénom')}}
                    {{Form::text('prenom', '', ['class' => 'form-control', 'placeholder' => 'Prénom'])}}

                    {{Form::label('nom', 'Nom')}}
                    {{Form::text('nom', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}

                    {{Form::label('sexe', 'Sexe')}}
                    {{Form::select('sexe', array('homme' => 'Homme', 'femme' => 'Femme', 'nonbinaire' => 'Non-binaire'), '', ['class' => 'form-control'])}}

                    {{Form::label('lieu_naissance', 'Lieu de naissance')}}
                    {{Form::text('lieu_naissance', '', ['class' => 'form-control', 'placeholder' => 'Lieu de naissance'])}}

                    {{Form::label('date_naissance', 'Date de naissance')}}
                    {{Form::date('date_naissance', '', ['class' => 'form-control', 'placeholder' => 'AAAA-MM-JJ'])}}

                    {{Form::label('telephone', 'Téléphone')}}
                    {{Form::text('telephone', '', ['class' => 'form-control', 'placeholder' => '1231231234'])}}

                    {{Form::label('departement', 'Departement')}}
                    {{Form::select('departement', $select,null, ['class' => 'form-control'])}}

                    {{Form::label('fonction', 'Fonction')}}
                    {{Form::select('fonction', $selectdeux,null, ['class' => 'form-control'])}}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    {{Form::label('adresse', 'Adresse')}}
                    {{Form::text('adresse', '', ['class' => 'form-control', 'placeholder' => 'Adresse'])}}

                    {{Form::label('app', 'Appartement')}}
                    {{Form::text('app', '', ['class' => 'form-control', 'placeholder' => 'Appartement'])}}

                    {{Form::label('ville', 'Ville')}}
                    {{Form::text('ville', '', ['class' => 'form-control', 'placeholder' => 'Ville'])}}

                    {{Form::label('province', 'Province')}}
                    {{Form::text('province', '', ['class' => 'form-control', 'placeholder' => 'Province'])}}

                    {{Form::label('code_postal', 'Code postal')}}
                    {{Form::text('code_postal', '', ['class' => 'form-control', 'placeholder' => 'g0g1h1'])}}
                </div>
            </div>
          </div>
            {!! Form::submit('Soumettre', null) !!}
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
@endsection
