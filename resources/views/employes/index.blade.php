@extends('layouts.app')

@section('main-content')
      <div class="container topOff">

        @include('flash::message')
        <a href="employes/create" class="btn btn-success">Créer un employé</a>
        <div class="row">
          <div class="col-sm-12">
            <div id="main">
                  <h3><label for="recherche">Rechercher un employé par nom de famille</label></h3>
                  <input class="form-control" name="recherche" placeholder="Entrez un nom de famille" type="text" v-model="search"/>
                  <div v-for="emp in filteredEmployes">
                   <!-- <span><img style="max-width:60px" :src="customer.profile_pic" class="profile-pic" /></span> -->
                       <div class="card">

                             <div class="card-block">
                               <p>
                                 departement: @{{emp.departement}}
                               </p>
                               <p>
                                 fonction: @{{emp.fonction}}
                               </p>
                               <h4 class="card-title">nom: @{{emp.nom}}</h4>

                               <a class="btn btn-info" :href="'/employes/'+emp.id">Consulter</a>

                           </div>

                       </div>
                    </div>
            </div>

          </div>
        </div>

      </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
      <script>
            var app = new Vue({
            el: "#main",
            data: function(){
                    return {
                    search: '',
                    resultats: {!! $resultat !!}
                   };
            },
            computed:
            {
                filteredEmployes:function()
                {
                  var self = this;
                  return this.resultats.filter(function(emp){return emp.nom.toLowerCase().indexOf(self.search.toLowerCase())>=0;});
                }
            }
            });

      </script>
@endsection
