@extends('layouts.auth')

@section('content')

<body>
    <div class="mytop-content" >
        <div class="container" >

                 <div class="col-sm-12 "  >
                   {{--
                   <a class="mybtn-social pull-right" href="{{ url('/register') }}">
                       Registre
                  </a>--}}

                  <a class="mybtn-social pull-right" href="{{ url('/login') }}">
                       Connexion
                  </a>

                </div>





            <div class="row">
              <div class="col-sm-6 col-sm-offset-3 myform-cont" >
                    <div class="myform-top">
                        <div class="myform-top-left">
                         <img  src="{{ url('img/logo_exportech.png') }} " class="img-responsive logo" />
                          <h3><strong>Entrez sur notre site Extranet.</strong></h3>
                            <p><strong>S'il vous plaît, saisissez votre courrier et mot de pas</strong></p>
                        </div>
                        <div class="myform-top-right">
                          <i class="fa fa-key"></i>
                        </div>
                    </div>

            @if (count($errors) > 0)
                 <div class="col-sm-12" >
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Erreur d'accès
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                </div>
                @endif
                    <div class="myform-bottom">

                      <form role="form" action="{{ url('/login') }}" method="post" >
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <input type="text" name="email" value="{{ old('email') }}" placeholder="Utilisateur..." class="form-control" id="form-username">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Mot de passe..." class="form-control"
                                   id="form-password">
                        </div>


                        {{-- <div class="form-group">
                          {!! Recaptcha::render() !!}
                        </div>--}}



                        <button type="submit" class="mybtn">Entrer</button>
                      </form>

                    </div>
              </div>
            </div>
            <div class="row">
                <div class="col-sm-12 mysocial-login">
                  <img  src="{{ url('img/slogan.png') }} " class="img-responsive logo" />
                    <h3>...Visite-nous dans notre site web...</h3>
                    <h1><strong>Exportech</strong></h1>

                </div>
            </div>
        </div>
      </div>

    <!-- Enlazamos el js de Bootstrap, y otros plugins que usemos siempre al final antes de cerrar el body -->
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
  </body>

@endsection
