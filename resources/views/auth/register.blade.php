 @extends('layouts.auth')

@section('content')

  <body>
    <div class="mytop-content" >
        <div class="container" >

                <div class="col-sm-12 " style="background-color:rgba(0, 0, 0, 0.35); height: 60px; " >
                   <a class="mybtn-social pull-right" href="{{ url('/register') }}">
                       Registre
                  </a>

                  <a class="mybtn-social pull-right" href="{{ url('/login') }}">
                       Connexion
                  </a>

                </div>

            <div class="row">
              <div class="col-sm-6 col-sm-offset-3 myform-cont" >


                     <div class="myform-top">
                        <div class="myform-top-left">
                           {{-- <img  src="{{ url('img/logo_plusis.png') }}" class="img-responsive logo" /> --}}
                          <h3>Inscrivez-vous dans notre Extranet</h3>
                            <p>S'il vous plaît, saisissez le données personnelles:</p>
                        </div>
                        <div class="myform-top-right">
                          <i class="fa fa-user"></i>
                        </div>
                      </div>

                  <div class="col-md-12" >
                    @if (count($errors) > 0)

                        <div class="alert alert-danger">
                            <strong>UPPS!</strong>une erreur après s'être inscrit<br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>

                    @endif
                   </div  >

                    <div class="myform-bottom">

                      <form role="form" action="{{ url('/register') }}" method="post" class="">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                        <input type="text" name="name" placeholder="Nom..." class="form-control" value="{{ old('name') }}" >
                        </div>

                        <div class="form-group">
                            <input type="text" name="email" placeholder="Courriel.." class="form-control"
                            value="{{ old('email') }}" />
                        </div>



                        <div class="form-group">
                        <input type="password" name="password" placeholder="Mot de passe..." class="form-control" >
                        </div>


                         <div class="form-group">
                        <input type="password" name="password_confirmation" placeholder="Mot de passe..."
                               class="form-control" >
                        </div>

                      {{--   <div class="form-group">
                         {!! Recaptcha::render() !!}
                        </div>--}}



                        <button type="submit" class="mybtn">M'inscrire</button>
                      </form>

                    </div>
              </div>
            </div>

            <div class="row">
                <div class="col-sm-12 mysocial-login">
                    <h3>...Visite-nous dans notre site web...</h3>
                    <h1><strong>Exportech Québec</strong></h1>

                </div>
            </div>
        </div>
      </div>

 </body>
@endsection
