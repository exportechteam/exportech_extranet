@extends('layouts.app')

@section('main-content')


        <div class="container">
            @include('flash::message')

            <h1>Modifier les données d'intégration de {{$coordonnees->prenom}} {{$coordonnees->nom}}</h1>

            {!! Form::open(['route' => ['integrations_update', $integration->participant_id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
            <div class="row">
              <div class="col-md-6">

                <div class="form-group">
                  {{ Form::label('photo', 'Photo') }}
                  <img src="{{ url('/img') }}/{{$integration->photo}}" title="photo" alt="photo" style="height:140px;width:140px;">
                  {{ Form::file('photo') }}

                  {{Form::label('numero_participant', 'Numéro du participant')}}
                  {{Form::text('numero_participant', $integration->numero_participant, ['class' => 'form-control', 'placeholder' => 'Numéro du participant'])}}

                  {{Form::label('urgence_contact', 'Urgence contact')}}
                  {{Form::text('urgence_contact', $integration->urgence_contact, ['class' => 'form-control', 'placeholder' => 'Urgence contact'])}}

                  {{Form::label('urgence_coordonnees', 'Urgence coordonnées')}}
                  {{Form::text('urgence_coordonnees', $integration->urgence_coordonnees, ['class' => 'form-control', 'placeholder' => 'Urgence coordonnées'])}}

                  {{Form::label('periode', 'Période')}}
                  {{Form::text('periode', $integration->periode, ['class' => 'form-control', 'placeholder' => 'Période'])}}

                  {{Form::label('date_debut_stage', 'Date de début du stage')}}
                  {{Form::date('date_debut_stage', $integration->date_debut_stage, ['class' => 'form-control', 'placeholder' => 'AAAA-MM-JJ'])}}

                  {{Form::label('nombre_semaine_prolongation', 'Nombre de semaine(s) de prolongation')}}
                  {{Form::text('nombre_semaine_prolongation', $integration->nombre_semaine_prolongation, ['class' => 'form-control', 'placeholder' => 'Nombre de semaine(s) de prolongation'])}}

                  {{Form::label('nombre_semaine_absence', "Nombre de semaine(s) d'absence")}}
                  {{Form::text('nombre_semaine_absence', $integration->nombre_semaine_absence, ['class' => 'form-control', 'placeholder' => "Nombre de semaine(s) d'absence"])}}

                  {{Form::label('nombre_semaine_totale', 'Nombre de semaine(s) totale(s)')}}
                  {{Form::text('nombre_semaine_totale', $integration->nombre_semaine_totale, ['class' => 'form-control', 'placeholder' => 'Nombre de semaine(s) totale(s)'])}}

                  {{Form::label('date_fin_prevue_stage', 'Date de fin prévue')}}
                  {{Form::date('date_fin_prevue_stage', $integration->date_fin_prevue_stage, ['class' => 'form-control', 'placeholder' => 'AAAA-MM-JJ'])}}

                  {{Form::label('date_fin_reelle_stage', 'Date de fin réelle')}}
                  {{Form::date('date_fin_reelle_stage', $integration->date_fin_reelle_stage, ['class' => 'form-control', 'placeholder' => 'AAAA-MM-JJ'])}}

                  {{Form::label('reference', 'Référence')}}
                  {{Form::text('reference', $integration->reference, ['class' => 'form-control', 'placeholder' => 'Référence'])}}

                  {{Form::label('clientele', 'Clientèle')}}
                  {{Form::text('clientele', $integration->clientele, ['class' => 'form-control', 'placeholder' => 'Clientèle'])}}

                  {{Form::label('source_revenu', 'Source de revenu')}}
                  {{Form::text('source_revenu', $integration->source_revenu, ['class' => 'form-control', 'placeholder' => 'Source de revenu'])}}

                  {{Form::label('financement', 'Financement')}}
                  {{Form::text('financement', $integration->financement, ['class' => 'form-control', 'placeholder' => 'Financement'])}}


                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  {{Form::label('no_dossier_emploiQC', 'Numéro dossier Emploi-Québec')}}
                  {{Form::text('no_dossier_emploiQC', $integration->no_dossier_emploiQC, ['class' => 'form-control', 'placeholder' => 'Numéro dossier Emploi-Québec'])}}

                  {{Form::label('nom_agent', "Nom de l'agent")}}
                  {{Form::text('nom_agent', $integration->nom_agent, ['class' => 'form-control', 'placeholder' => "Nom de l'agent"])}}

                  {{Form::label('adresse_agent', "Adresse de l'agent")}}
                  {{Form::text('adresse_agent', $integration->adresse_agent, ['class' => 'form-control', 'placeholder' => "Adresse de l'agent"])}}

                  {{Form::label('provenance', 'Provenance')}}
                  {{Form::text('provenance', $integration->provenance, ['class' => 'form-control', 'placeholder' => 'Provenance'])}}

                  {{Form::label('particularite', 'Particularité')}}
                  {{Form::text('particularite', $integration->particularite, ['class' => 'form-control', 'placeholder' => 'Particularité'])}}

                  {{Form::label('transport', 'Transport')}}
                  {{Form::text('transport', $integration->transport, ['class' => 'form-control', 'placeholder' => 'Transport'])}}

                  {{Form::label('commentaires', 'Commentaires')}}
                  {{Form::text('commentaires', $integration->commentaires, ['class' => 'form-control', 'placeholder' => 'Commentaires'])}}

                  {{Form::label('raison_depart', 'Raison départ')}}
                  {{Form::text('raison_depart', $integration->raison_depart, ['class' => 'form-control', 'placeholder' => 'Raison départ'])}}

                  {{Form::label('en_emploi', 'En emploi')}}
                  {{Form::text('en_emploi', $integration->en_emploi, ['class' => 'form-control', 'placeholder' => 'En emploi'])}}

                  {{Form::label('objectif_atteint', 'Objectif atteint')}}
                  {{Form::text('objectif_atteint', $integration->objectif_atteint, ['class' => 'form-control', 'placeholder' => 'Objectif atteint'])}}
                </div>
            </div>
          </div>
            {!! Form::submit('Soumettre', null) !!}
            {!! Form::close() !!}
            @include('inc.messages')
            <a href="/">Annuler</a>
        </div>
@endsection
