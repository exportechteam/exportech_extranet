@extends('layouts.app')

@section('main-content')


      <div class="topOff container bottomOff">
        <!--pour admin et coordonateur-->
        <a href="/integration/edit/{{$participants->id}}" class="btn btn-success">Modifier intégration</a>
        <div class="row">
          <div class="col-md-6">
            <h1>{{$coordonnees->nom}}, {{$coordonnees->prenom}}</h1>
            <img src="{{ url('/img') }}/{{$integration->photo}}" title="photo" alt="photo" style="height:140px;width:140px;">

            <p>Numéro du participant : {{$integration->numero_participant}}</p>

            <p>Urgence contact : {{$integration->urgence_contact}}</p>

            <p>Urgence coordonnées : {{$integration->urgence_coordonnees}}</p>

            <p>Période : {{$integration->periode}}</p>

            <p>Date de début du stage : {{$integration->date_debut_stage}}</p>

            <p>Nombre de semaine(s) de prolongation : {{$integration->nombre_semaine_prolongation}}</p>

            <p>Nombre de semaine(s) d'absence : {{$integration->nombre_semaine_absence}}</p>

            <p>Nombre de semaine(s) totale(s) : {{$integration->nombre_semaine_totale}}</p>

            <p>Date de fin prévue : {{$integration->date_fin_prevue_stage}}</p>

            <p>Date de fin réelle : {{$integration->date_fin_reelle_stage}}</p>

            <p>Référence : {{$integration->reference}}</p>

            <p>Clientèle : {{$integration->clientele}}</p>

            <p>Source de revenu : {{$integration->source_revenu}}</p>

            <p>Financement : {{$integration->financement}}</p>

          </div>

        <div class="col-md-6">
          <div class="form-group">
            <p>Numéro dossier Emploi-Québec : {{$integration->no_dossier_emploiQC}}</p>

            <p>Nom de l'agent : {{$integration->nom_agent}}</p>

            <p>Adresse de l'agent : {{$integration->adresse_agent}}</p>

            <p>Provenance : {{$integration->provenance}}</p>

            <p>Particularité : {{$integration->particularite}}</p>

            <p>Transport : {{$integration->transport}}</p>

            <p>Commentaires : {{$integration->commentaires}}</p>

            <p>Raison départ : {{$integration->raison_depart}}</p>

            <p>En emploi : {{$integration->en_emploi}}</p>

            <p>Objectif atteint : {{$integration->objectif_atteint}}</p>


            <a href="/" class="btn btn-info">Retour à la page principale</a>

          </div>
        </div>
      </div>

      </div>
@endsection
