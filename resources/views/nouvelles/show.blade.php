@extends('layouts.app')

@section('main-content')

      <div class="topOff container">
        <!--pour admin et coordonateur-->
        <a href="{{$nouvelles->id}}/edit" class="btn btn-success">Modifier</a>
        {!! Form::open(['route' => ['nouvelles.destroy', $nouvelles->id], 'method' => 'DELETE']) !!}
                        <div class="form-group">

                            {!! Form::hidden('_method','DELETE') !!}
                            {!! Form::submit('Supprimer', ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Êtes-vous sûr de vouloir supprimer cette nouvelle?")']) !!}


                        </div>
                    {!! Form::close() !!}
        <div class="row">
          <div class="col-md-12">

            <h1>{{$nouvelles->titre}}</h1>
            <img src="/img/{{$nouvelles->image}}" alt="image_article" />

          </div>
          <div class="col-md-12 topOff bottomOff">
            <p>
              {!!$nouvelles->contenu!!}
            </p>
            <p>
              Publiée: {{$nouvelles->created_at}}
            </p>

            <a href="/" class="btn btn-info">Retour à la page principale</a>

          </div>
        </div>

      </div>

@endsection
