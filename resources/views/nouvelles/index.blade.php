@extends('layouts.app')

@section('main-content')
      <div class="hero jumbotron jumbotron-fluid">
        <div class="container">
          <div class="flex-center position-ref full-height">


            <div class="title m-b-md">
              <h1>
                EXPORTECH QUÉBEC
              </h1>
            </div>


          </div>
        </div>

      </div>
      <div class="container">
        @include('flash::message')
        <a href="nouvelles/create" class="btn btn-success">Créer une nouvelle</a>
        <div class="row">
          <div class="col-sm-12">
            @foreach($nouvelles as $nouvelle)
            <div class="card">
              <div class="row">
                <div class="col-md-7 col-sm-12">
                  <div class="card-block">
                    <p>
                      Publiée: {{$nouvelle->created_at}}
                    </p>
                    <h4 class="card-title">{{$nouvelle->titre}}</h4>
                    <p class="card-text">{!!str_limit($nouvelle->contenu, 150)!!} </p>
                    <a href="nouvelles/{{$nouvelle->id}}" class="btn btn-info">Lire l'article</a>

                  </div>
                </div>
                <div class="col-md-5 col-sm-12">
                  <div class="card-img-bottom">
                    <img src="img/{{$nouvelle->image}}" alt="" />
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            </div>

          </div>

      </div>
@endsection
