@extends('layouts.app')

@section('main-content')

        <div class="container">
            @include('flash::message')

            <h1>Modifier une nouvelle</h1>



            {!! Form::open(['route' => ['nouvelles.update', $nouvelles->id], 'enctype' => 'multipart/form-data', 'method' => 'PUT']) !!}
                <div class="form-group">
                    {{Form::label('titre', 'Titre')}}
                    {{Form::text('titre', $nouvelles->titre, ['class' => 'form-control', 'placeholder' => 'Titre'])}}

                    {{Form::label('contenu', 'Contenu')}}
                    {{Form::textarea('contenu', $nouvelles->contenu, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Contenu'])}}

                    {{ Form::label('image', 'Image') }}
                    {{ Form::file('image', '') }}

                    {!! Form::hidden('_method','PUT') !!}
                    {!! Form::submit('Soumettre', '') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
            <a href="/">Annuler</a>
        </div>

@endsection
