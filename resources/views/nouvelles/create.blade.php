@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Ajouter une nouvelle</h1>
            {!! Form::open(['route' => 'nouvelles.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('titre', 'Titre')}}
                    {{Form::text('titre', '', ['class' => 'form-control', 'placeholder' => 'Titre'])}}

                    {{Form::label('contenu', 'Contenu')}}
                    {{Form::textarea('contenu', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Contenu'])}}

                    {{ Form::label('image', 'Image') }}
                    {{ Form::file('image') }}

                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">

        </script>
@endsection
