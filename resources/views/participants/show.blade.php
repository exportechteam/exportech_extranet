@extends('layouts.app')

@section('main-content')

      <div class="topOff container bottomOff">
        <!--pour admin et coordonateur-->
        <a href="{{$participants->id}}/edit" class="btn btn-success">Modifier</a>
        <div class="row">
          <div class="col-md-12">
            @foreach($coordonnees as $coor)
            @if($coor->id == $participants->coordonnees_id)
            <h1>{{$coor->nom}}, {{$coor->prenom}}</h1>
            @endif
            @endforeach

            @foreach($coordonnees as $coor)
              @if($coor->id == $participants->coordonnees_id)
            <p>
              Courriel: {{$coor->email}}
            </p>
            <p>
              Sexe: {{$coor->sexe}}
            </p>
            <p>
              Lieu de naissance: {{$coor->lieu_naissance}}
            </p>
            <p>
              Date de naissance: {{$coor->date_naissance}}
            </p>

                <p>
                  Adresse: {{$coor->adresse}}
                </p>
                <p>
                  Appartement: {{$coor->appartement}}
                </p>
                <p>
                  Ville: {{$coor->ville}}
                </p>
                <p>
                  Province: {{$coor->province}}
                </p>
                <p>
                  Code postal: {{$coor->code_postal}}
                </p>
                <p>
                  Téléphone: {{$coor->telephone}}
                </p>
                <p>
                  Créé: {{$coor->created_at}}
                </p>
              @endif
            @endforeach

            <a href="/" class="btn btn-info">Retour à la page principale</a>

          </div>
        </div>

      </div>
@endsection
