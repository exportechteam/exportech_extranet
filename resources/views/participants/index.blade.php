@extends('layouts.app')

@section('main-content')


      <div class="container topOff">

        @include('flash::message')
        <a href="participants/create" class="btn btn-success">Créer un participant</a>
        <div class="row">
          <div class="col-sm-12">

            <div id="main">
            <h3><label for="recherche">Rechercher un participant par nom de famille ou prénom</label></h3>
            <input class="form-control" name="recherche" placeholder="Entrez un nom de famille ou prénom" type="text" v-model="search"/>
            <div v-for="part in filteredParticipants">
             <!-- <span><img style="max-width:60px" :src="customer.profile_pic" class="profile-pic" /></span> -->
                 <div class="card">

                       <div class="card-block">
                         <p>
                           Nom: @{{part.nom}}
                         </p>
                         <p>
                           Niveau études: @{{part.niveau_etudes}}
                         </p>
                         <p>
                           Domaine études: @{{part.domaine_etudes}}
                         </p>
                         <h4 class="card-title"></h4>

                         <a class="btn btn-info" :href="'/participants/'+part.id">Consulter</a>
                         <p>
                         <div v-if="part.admis_personne == 0">
                           <a class="btn btn-info" :href="'/integration/create/'+part.id">Intégrer</a>
                         </div>
                         <div v-else>
                           <a class="btn btn-info" :href="'/integration/edit/'+part.id">Modification intégration</a>
                         </div>
                       </div>

                 </div>
              </div>
      </div>

    </div>
  </div>
  </div>

    <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
      <script>
            var app = new Vue({
            el: "#main",
            data: function(){
                    return {
                    search: '',
                    resultats: {!! $resultat !!}
                   };
            },
            computed:
            {
                filteredParticipants:function()
                {
                  var self = this;
                  return this.resultats.filter(function(part){return part.nom.toLowerCase().indexOf(self.search.toLowerCase())>=0;});
                }
            }
            });

      </script>

@endsection
