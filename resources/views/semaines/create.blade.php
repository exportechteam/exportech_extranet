@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Semaine no. {{$no_semaine}}</h1>
            <h3>La barre de progrès doit atteindre 100% pour que vous puissiez terminer la semaine. Donnez un pourcentage aux compétences que vous avez travaillé cette semaine.<br><br> <span style="color: red;">Pensez également à cocher les autres éléments que vous avez travaillé cette semaine.</span></h3>
            <div class="progress">
              <div id="progres" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">
              </div>
            </div>
            {!! Form::open(['route' => 'semaines.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}

                <div class="form-group">
                  <div class="row">

                    {{Form::label('elements', 'Elements', ['class' => 'form-control'])}}

                    @foreach($sections as $section)
                      <div class="col-md-12">
                        <h3>{{$section->nom_section}}</h3>
                      </div>

                      @foreach($elements as $element)
                      @if($element[0]->section_id == $section->id)
                      @if($section->nom_section == "Competences")
                      <div class="col-md-4" style="color: green;">
                          {{Form::selectRange($element[0]->id, '0','100','0',['class' => 'leTotal'])}}
                          <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                      </div>
                      @elseif($section->nom_section == "Taches obligatoires")
                        <div class="col-md-6" style="color: orange;">
                          {{Form::checkbox($element[0]->id, '',false,['class' => 'form-check-input'])}}
                          <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                        </div>
                      @elseif($section->nom_section == "Mandats")
                      <div class="col-md-6" style="color: gold;">
                        {{Form::checkbox($element[0]->id, '',false,['class' => 'form-check-input'])}}
                        <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                      </div>
                      @elseif($section->nom_section == "MDRE")
                      <div class="col-md-6" style="color: blue;">
                        {{Form::checkbox($element[0]->id, '',false,['class' => 'form-check-input'])}}
                        <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                      </div>
                      @elseif($section->nom_section == "Atelier")
                      <div class="col-md-6" style="color: purple;">
                        {{Form::checkbox($element[0]->id, '',false,['class' => 'form-check-input'])}}
                        <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                      </div>
                      @else
                      <div class="col-md-6">
                        {{Form::checkbox($element[0]->id, '',false,['class' => 'form-check-input'])}}
                        <span class="form-check-label">{{$element[0]->nom_elements}}</span>
                      </div>
                        @endif

                      @endif
                      @endforeach
                    @endforeach
                    {{Form::hidden('no_semaine', $no_semaine)}}
                    {{Form::hidden('id_du_participant', $id_du_participant)}}


                  </div>








                    {!! Form::submit('Soumettre', ['class' => 'form-control']) !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="{{ URL::to('/js/main.js') }}"></script>

@endsection
