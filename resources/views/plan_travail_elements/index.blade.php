@extends('layouts.app')

@section('main-content')
      <div class="container topOff">

        @include('flash::message')
        <a href="/plan_travail_elements/create" class="btn btn-success">Créer un élément</a>
        <div class="row">
          <div class="col-sm-12">
            <div id="main">
                  <h3><label for="recherche">Rechercher un élément de plan de travail</label></h3>
                  <input class="form-control" name="recherche" placeholder="Entrez le nom de l'élément" type="text" v-model="search"/>
                  <div v-for="element in filteredElements">
                   <!-- <span><img style="max-width:60px" :src="customer.profile_pic" class="profile-pic" /></span> -->
                       <div class="card">

                             <div class="card-block">
                               <p>
                                 departement: @{{element.departement}}
                               </p>
                               <p>
                                 section: @{{element.section}}
                               </p>
                               <h4 class="card-title">nom: @{{element.nom}}</h4>

                               <a class="btn btn-info" :href="'/plan_travail_elements/'+element.id">Consulter</a>

                           </div>

                       </div>
                    </div>
            </div>

          </div>
        </div>

      </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
      <script>
            var app = new Vue({
            el: "#main",
            data: function(){
                    return {
                    search: '',
                    resultats: {!! $resultat !!}
                   };
            },
            computed:
            {
                filteredElements:function()
                {
                  var self = this;
                  return this.resultats.filter(function(element){return element.nom.toLowerCase().indexOf(self.search.toLowerCase())>=0;});
                }
            }
            });

      </script>
@endsection
