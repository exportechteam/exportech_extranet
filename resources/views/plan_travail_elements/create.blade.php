@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Ajouter un élément de plan de travail</h1>
            {!! Form::open(['route' => 'plan_travail_elements.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
                <div class="form-group">
                    {{Form::label('nom_elements', 'Nom élément')}}
                    {{Form::text('nom_elements', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}

                    {{Form::label('description_element', 'Description élément')}}
                    {{Form::text('description_element', '', ['class' => 'form-control', 'placeholder' => 'Desc'])}}

                    {{Form::label('departement', 'Département')}}
                    {{Form::select('departement', $select, null, ['class' => 'form-control'])}}

                    {{Form::label('section', 'Section du plan')}}
                    {{Form::select('section', $selectDeux, null, ['class' => 'form-control'])}}

                    {!! Form::submit('Soumettre') !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js">

        </script>
@endsection
