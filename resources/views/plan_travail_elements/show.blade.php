@extends('layouts.app')

@section('main-content')
      <div class="topOff container">
        <div class="row">

            <div class="col-md-12">
              <h2>{{$elements->nom_elements}}</h2>
              <small>Créé le: {{$elements->created_at}}</small>
              <p>
                Département: {{$departement[0]->nom_departement}}
              </p>
              <p>
                Section du plan: {{$section[0]->nom_section}}
              </p>


            </div>

        </div>
        <a class="btn btn-success" href="/plan_travail_elements/{{$elements->id}}/edit">Modifier</a>
        {!! Form::open(['route' => ['plan_travail_elements.destroy', $elements->id], 'method' => 'DELETE']) !!}
          <div class="form-group">

            {!! Form::hidden('_method','DELETE') !!}
            {!! Form::submit('Rendre élément inactif', ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Êtes-vous sûr de vouloir supprimer ce plan?")']) !!}


          </div>
        {!! Form::close() !!}

      </div>
@endsection
