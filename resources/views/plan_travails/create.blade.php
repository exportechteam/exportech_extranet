@extends('layouts.app')

@section('main-content')

        <div class="container">
            <!-- @include('flash::message') -->

            <h1>Créer un plan de travail</h1>
            {!! Form::open(['route' => 'plan_travails.store', 'enctype' => 'multipart/form-data', 'method' => 'POST']) !!}
                <div class="form-group">
                  <div class="row">

                    {{Form::label('participant', 'Participant')}}

                    {{Form::select('participant', $select,null, ['class' => 'form-control'])}}

                    {{Form::label('elements', 'Elements', ['class' => 'form-control'])}}

                    @foreach($sections as $section)
                      <div class="col-md-12">
                        <h3>{{$section->nom_section}}</h3>
                      </div>

                      @foreach($elements as $element)
                      @if($element->section_id == $section->id)
                      <div class="col-md-4">
                        {{Form::checkbox('elements[]', $element->id,true,['class' => 'form-check-input'])}}
                        <span class="form-check-label">{{$element->nom_elements}}</span>
                      </div>
                      @endif
                      @endforeach
                    @endforeach



                  </div>



                    {!! Form::submit('Soumettre', ['class' => 'form-control']) !!}


                </div>
            {!! Form::close() !!}
            @include('inc.messages')
        </div>
@endsection
