@extends('layouts.app')

@section('main-content')
      <div class="topOff container">
        <div class="row">
            <a class="btn btn-info" href="/semaines/create/{{$plans->participant_id}}">Nouvelle semaine</a>

            <h1>Plan de travail</h1>

            <h2>Statistiques par semaine</h2>
            <div class="row">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($semaines as $sem)

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$sem->no_semaine}}">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$sem->no_semaine}}" aria-expanded="false" aria-controls="collapse{{$sem->no_semaine}}">
                          Semaine #{{$sem->no_semaine}}
                        </a>
                      </h4>
                    </div>
                    <div id="collapse{{$sem->no_semaine}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$sem->no_semaine}}">
                      <div class="panel-body">
                        <table class="table table-bordered">
                          <tr>
                            <th>
                              Nom de l'élément
                            </th>
                            <th>
                              Développement
                            </th>
                          </tr>

                  @foreach($sem->PlanTravailElements as $element)

                    @if($element->section_id == 2)
                        <tr>
                          <td>
                            {{$element->nom_elements}}
                            <span style="color: grey;"><i class="glyphicon glyphicon-arrow-right"></i>{{$element->section->nom_section}}</span>
                          </td>
                          <td>
                            {{str_limit($element->pivot->pourcentage, 6)}}%
                          </td>
                        </tr>
                    @else
                    <tr>
                      <td>
                        {{$element->nom_elements}}
                        @if($element->section->nom_section == "Mandats")
                          <span style="color: gold;"><i class="glyphicon glyphicon-arrow-right"></i>{{$element->section->nom_section}}</span>
                        @else
                          <span style="color: grey;"><i class="glyphicon glyphicon-arrow-right"></i>{{$element->section->nom_section}}</span>
                        @endif
                      </td>
                      <td style="color: green;">
                        Élément développé durant cette semaine.
                      </td>
                    </tr>
                    @endif
                  @endforeach
                    </table>
                  </div>
                </div>
                </div>

                @endforeach
                </div>

            </div>
            <div>
              <h3>Total des semaines</h3>
              <h4>Compétences</h4>

          @if(isset($totaux))
                @foreach($totaux as $key => $value)
                @foreach($elements as $element)
                  @if($element[0]->id == $key)
                    <label>{{$element[0]->nom_elements}}</label>

                  @endif
                @endforeach
                <div class="progress">
                  <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="{{$value/$semaines->count()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$value/$semaines->count()}}%;">
                    {{str_limit($value/$semaines->count(),6)}}%
                  </div>
                </div>
                @endforeach
            @endif

            </div>

            <div>
              <table class="table table-striped">
                <h3>Éléments non-travaillés</h3>
                <th>
                  Nom
                </th>
                <th>
                  Statut
                </th>


              @foreach($diff2 as $key => $value)
                @foreach($elements as $element)
                  @if($element[0]->id == $key)

                  <tr>
                    <td>
                      {{$element[0]->nom_elements}}
                    </td>
                    <td style="color:red;">
                      Vous n'avez pas encore travaillé cet élément dans vos semaines.
                    </td>
                  </tr>
                  @endif
                @endforeach
              @endforeach
              </table>
            </div>

        </div>





        <a class="btn btn-success" href="/plan_travails/{{$plans->id}}/edit">Modifier</a>

      </div>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      <script language="JavaScript" type="text/javascript" src="{{ URL::to('/js/plantravail.js') }}"></script> -->
@endsection
