
/*
 * Cette fonction affiche le formualire avec ajax pour editer les donnéées d'un utilisateurs
 */
function  voirinfo_utilisateur(arg){
  var urlracine=$("#url_racine_projet").val();
  var monurl =urlracine+"/form_editer_utilisateur/"+arg+"";
  $("#cape_modal").show();
	$("#cape_formulaires").show();
	var screenTop = $(document).scrollTop();
	$("#cape_formulaires").css('top', screenTop);
  $("#cape_formulaires").html($("#statut").html());

    $.ajax({
    url: monurl
    }).done( function(resul)
    {
     $("#cape_formulaires").html(resul);

    }).fail( function()
   {
    $("#cape_formulaires").html("<span>...Une erreur est survenue pendant l'operation1, révisez sa connexion et essayer à nouveau...</span>");
   }) ;
}


/*
 * Cette fonction ferme une fenetre modal
 */
$(document).on("click",".div_modal", function(e){
	$(this).hide();
	$("#cape_formulaires").hide();
	$("#cape_formulaires").html("");
    location.reload();

});



/*
 * Cette fonction charge trois formulaire avec ajax pour  editer un utilisateur
 */
function chager_formulaire(arg){
   var urlracine=$("#url_racine_projet").val();
   $("#cape_modal").show();
   $("#cape_formulaires").show();
   var screenTop = $(document).scrollTop();
   $("#cape_formulaires").css('top', screenTop);
   $("#cape_formulaires").html($("#statut").html());
   if(arg==1){ var monurl=urlracine+"/form_nouveau_utilisateur"; }
   if(arg==2){ var monurl=urlracine+"/form_nouveau_role"; }
   if(arg==3){ var monurl=urlracine+"/form_nouveau_permis"; }

    $.ajax({
    url: monurl
    }).done( function(resul)
    {
     $("#cape_formulaires").html(resul);

    }).fail( function()
   {
    $("#cape_formulaires").html("<span>...Une erreur est survenue pendant l'operation, révisez sa connexion et essayer à nouveau...</span>");
   }) ;

}


/*
 * Cette fonction crée un utilisateur, rôle, permission,
 * édite un utilisateur, l'access et élimine un utilisateur de la base de donnéées
 */
$(document).on("submit",".formentree",function(e){
  e.preventDefault();
  var qui=$(this).attr("id");
  var formu=$(this);
  var varurl="";

    if(qui=="f_creer_utilisateur"){  var varurl=$(this).attr("action");  var div_resul="cape_formulaires";  }
    if(qui=="f_creer_role"){  var varurl=$(this).attr("action");  var div_resul="cape_formulaires";  }
    if(qui=="f_creer_permission"){  var varurl=$(this).attr("action");  var div_resul="cape_formulaires";  }
    if(qui=="f_editer_utilisateur"){  var varurl=$(this).attr("action");  var div_resul="notification_E2";  }
    if(qui=="f_editer_acces"){  var varurl=$(this).attr("action");  var div_resul="notification_E3";  }
    if(qui=="f_supprimer_utilisateur"){  var varurl=$(this).attr("action");  var div_resul="cape_formulaires";  }
    if(qui=="f_assigner_permission"){  var varurl=$(this).attr("action");  var div_resul="cape_formulaires";  }

  $("#"+div_resul+"").html( $("#statut").html());

  $.ajax({
    // la URL para la petición
    url : varurl,
    data : formu.serialize(),
    type : 'POST',
    dataType : 'html',

    success : function(resul) {
      $("#"+div_resul+"").html(resul);

    },
    error : function(xhr, status) {
        $("#"+div_resul+"").html("<span>...Une erreur est survenue pendant l'operation, révisez sa connexion et essayer à nouveau...</span>");
    }
  });

});


/*
 * Cette fonction assigne un role avec ajax
 */
function assigner_role(idusu){
   var idrol=$("#rol1").val();
   var urlracine=$("#url_racine_projet").val();
   $("#zone_etiquettes_role").html($("#statut").html());
   var monurl=urlracine+"/assigner_role/"+idusu+"/"+idrol+"";

    $.ajax({
    url: monurl
    }).done( function(resul)
    {
      var etiquettes="";
      var role=$.parseJSON(resul);
      $.each(role,function(index, value) {
        etiquettes+= '<span class="label label-warning">'+value+'</span> ';
      });

     $("#zone_etiquettes_role").html(etiquettes);

    }).fail( function()
    {
    $("#zone_etiquettes_role").html("<span style='color:red;'>...Erreur: vous navez pas ajouté encore un rôle o ou révisez sa connexion...</span>");
    }) ;

}

/*
 * Cette fonction élimine un role
 */
function eliminer_role(idusu){
   var idrol=$("#rol2").val();
   var urlracine=$("#url_racine_projet").val();
   $("#zone_etiquettes_role").html($("#statut").html());
   var monurl=urlracine+"/eliminer_role/"+idusu+"/"+idrol+"";

    $.ajax({
    url: monurl
    }).done( function(resul)
    {
      var etiquettes="";
      var role=$.parseJSON(resul);
      $.each(role,function(index, value) {
        etiquettes+= '<span class="label label-warning" style="margin-left:10px;" >'+value+'</span> ';
      });

     $("#zone_etiquettes_role").html(etiquettes);

    }).fail( function()
    {
    $("#zone_etiquettes_role").html("<span style='color:red;'>...Error: vous n'avez pas ajouté encore un rôle o ou révisez sa connexion...</span>");
    }) ;
}


/*
 * Cette fonction élimine un utilisateurs
 */
function eliminer_utilisateur(idusu){

   var urlracine=$("#url_racine_projet").val();
   $("#cape_modal").show();
   $("#cape_formulaires").show();
   var screenTop = $(document).scrollTop();
   $("#cape_formulaires").css('top', screenTop);
   $("#cape_formulaires").html($("#statut").html());
   var monurl=urlracine+"/form_supprimer_utilisateur/"+idusu+"";


    $.ajax({
    url: monurl
    }).done( function(resul)
    {
     $("#cape_formulaires").html(resul);

    }).fail( function()
   {
    $("#cape_formulaires").html("<span>...Une erreur est survenue pendant l'operation, révisez sa connexion et essayer à nouveau...</span>");
   }) ;


}

/*
 * Cette fonction élimine une permission
 */
function eliminer_permis(idrol,idper){
     var urlracine=$("#url_racine_projet").val();
     var monurl=urlracine+"/supprimer_permission/"+idrol+"/"+idper+"";
     $("#filaP_"+idper+"").html($("#statut").html() );
        $.ajax({
            url: monurl
        }).done( function(resul)
    					{
                            $("#filaP_"+idper+"").hide();

    					}).fail( function()
   						{
     						alert("la permission n'a pas été correctement supprimée, essayer à nouveau ou révisez sa connexion");
   						});
}


/*
 * Cette fonction élimine un rôle
 */
function borrar_rol(idrol){

     var urlracine=$("#url_racine_projet").val();
     var monurl=urlracine+"/supprimer_role/"+idrol+"";
     $("#filaR_"+idrol+"").html($("#statut").html() );
        $.ajax({
    url: monurl
    }).done( function(resul)
    {
     $("#filaR_"+idrol+"").hide();

    }).fail( function()
   {
     alert("le rôle n'a pas été correctement supprimé, essayer à nouveau ou révisez sa connexion");
   }) ;

}
