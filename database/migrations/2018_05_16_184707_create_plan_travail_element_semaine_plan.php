<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTravailElementSemainePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('plan_travail_element_semaine_plan', function (Blueprint $table) {
        $table->increments('id');

        $table->integer('element_id')->unsigned();
        $table->foreign('element_id')->references('id')->on('plan_travail_elements');

        $table->integer('semaine_plan_id')->unsigned();
        $table->foreign('semaine_plan_id')->references('id')->on('semaine_plans');

        $table->decimal('pourcentage', 8, 2)->nullable();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
