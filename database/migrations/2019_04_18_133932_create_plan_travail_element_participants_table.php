<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTravailElementParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_travail_element_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::table('plan_travail_element_participants', function (Blueprint $table) {
            $table->unsignedInteger('element_id');
            $table->foreign('element_id')->references('id')->on('plan_travail_elements');
            $table->unsignedInteger('participant_id');
            $table->foreign('participant_id')->references('id')->on('participant_integrations');
  
  
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_travail_element_participants');
    }
}
