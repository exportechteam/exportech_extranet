<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_integrations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('photo', 400);
            $table->integer('numero_participant');
            $table->string('urgence_contact', 255);
            $table->string('urgence_coordonnees', 255);
            $table->string('periode', 255);
            $table->date('date_debut_stage');
            $table->integer('nombre_semaine_prolongation');
            $table->integer('nombre_semaine_absence');
            $table->integer('nombre_semaine_totale');
            $table->date('date_fin_prevue_stage');
            $table->date('date_fin_reelle_stage');
            $table->text('reference');
            $table->text('clientele');
            $table->string('source_revenu', 400);
            $table->string('financement', 400);
            $table->integer('no_dossier_emploiQC');
            $table->string('nom_agent', 400);
            $table->string('adresse_agent', 400);
            $table->string('provenance', 400);
            $table->text('particularite');
            $table->string('transport', 400);
            $table->text('commentaires');
            $table->text('raison_depart');
            $table->boolean('en_emploi');
            $table->boolean('objectif_atteint');
            $table->timestamps();
        });

        Schema::table('participant_integrations', function (Blueprint $table) {
          $table->unsignedInteger('participant_id');
          $table->foreign('participant_id')->references('id')->on('participants');

          $table->unsignedInteger('departementIntegration_id');
          $table->foreign('departementIntegration_id')->references('id')->on('departements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_integrations');
    }
}
