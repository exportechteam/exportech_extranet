<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->tinyInteger('isAdmin');
            $table->timestamps();
        });

        Schema::table('employes', function (Blueprint $table) {
          $table->unsignedInteger('departement_id');
          $table->foreign('departement_id')->references('id')->on('departements');

          $table->unsignedInteger('fonction_id');
          $table->foreign('fonction_id')->references('id')->on('fonctions');

          $table->unsignedInteger('coordonnees_id');
          $table->foreign('coordonnees_id')->references('id')->on('coordonnees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
