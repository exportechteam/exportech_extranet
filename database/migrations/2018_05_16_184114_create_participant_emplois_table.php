<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantEmploisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_emplois', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('status_emploi', 255);
            $table->string('titre_emploi', 255);
            $table->string('entreprise_emploi', 255);
            $table->string('personne_ressource_emploi', 255);
            $table->string('courriel_emploi', 255);
            $table->string('adresse_emploi', 255);
            $table->string('suite_emploi', 255);
            $table->string('ville_emploi', 255);
            $table->string('province_emploi', 255);
            $table->string('code_postal_emploi', 255);
            $table->string('telephone', 30);
            $table->string('site_web_emploi', 400);
            $table->date('date_debut_emploi');
            $table->text('commentaires_emploi');
            $table->timestamps();
        });

        Schema::table('participant_emplois', function (Blueprint $table) {
        $table->unsignedInteger('participantEmploi_id');

        $table->foreign('participantEmploi_id')->references('id')->on('plan_travail_elements');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_emplois');
    }
}
