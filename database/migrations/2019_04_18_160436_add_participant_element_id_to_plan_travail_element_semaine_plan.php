<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParticipantElementIdToPlanTravailElementSemainePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_travail_element_semaine_plan', function (Blueprint $table) {
            $table->unsignedInteger('participant_element_id');
            $table->foreign('participant_element_id')->references('id')->on('plan_travail_element_participants');
  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
