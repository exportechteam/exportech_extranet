<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemainePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semaine_plans', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('no_semaine');
            $table->boolean('complete');
            $table->timestamps();
        });

        Schema::table('semaine_plans', function (Blueprint $table) {
          $table->unsignedInteger('participant_id');

          $table->foreign('participant_id')->references('id')->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semaine_plans');
    }
}
