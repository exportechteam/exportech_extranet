<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuivisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suivis', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('date_suivi');
            $table->text('notes_suivi');
            $table->timestamps();
        });
        Schema::table('suivis', function (Blueprint $table) {
          $table->unsignedInteger('participantSuivi_id');
          $table->foreign('participantSuivi_id')->references('id')->on('participant_integrations');

          $table->unsignedInteger('employeResponsable_id');
          $table->foreign('employeResponsable_id')->references('id')->on('employes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suivis');
    }
}
