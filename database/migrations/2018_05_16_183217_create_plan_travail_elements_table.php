<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanTravailElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_travail_elements', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nom_elements', 255);
            $table->text('description_element');
            $table->boolean('actif_element');
            $table->timestamps();
        });

        Schema::table('plan_travail_elements', function (Blueprint $table) {
          $table->unsignedInteger('departement_id');
          $table->foreign('departement_id')->references('id')->on('departements');

          $table->unsignedInteger('section_id');
          $table->foreign('section_id')->references('id')->on('plan_travail_sections');

          // $table->unsignedInteger('plan_id');
          // $table->foreign('plan_id')->references('id')->on('plan_travails');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_travail_elements');
    }
}
