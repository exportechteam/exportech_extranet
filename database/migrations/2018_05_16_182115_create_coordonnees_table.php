<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordonneesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordonnees', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('prenom', 255);
            $table->string('nom', 255);
            $table->string('sexe', 255);
            $table->string('lieu_naissance', 255);
            $table->date('date_naissance');
            $table->string('adresse', 400);
            $table->string('appartement', 100);
            $table->string('ville', 255);
            $table->string('province', 255);
            $table->string('code_postal', 30);
            $table->string('telephone', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordonnees');
    }
}
