<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAteliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ateliers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nom_atelier', 255);
            $table->text('description_atelier');
            $table->date('date_atelier');
            $table->timestamps();
        });

        Schema::table('ateliers', function (Blueprint $table) {
        $table->unsignedInteger('participantAtelier_id');
        $table->foreign('participantAtelier_id')->references('id')->on('participant_integrations');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ateliers');
    }
}
