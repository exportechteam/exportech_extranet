<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index');
    
  // Gestion des acces avec Shinobi pour la liste des utilisateurs  
  Route::get('/liste_utilisateur', 'UtilisateurController@lister_utilisateur')->name('users.index')
                                ->middleware('permissionshinobi:users.index');

  Route::post('creer_utilisateur', 'UtilisateurController@creer_utilisateur');
  Route::post('editer_utilisateur', 'UtilisateurController@editer_utilisateur');
  Route::post('chercher_utilisateur', 'UtilisateurController@chercher_utilisateur');
  Route::post('supprimer_utilisateur', 'UtilisateurController@supprimer_utilisateur');
  Route::post('editer_acces', 'UtilisateurController@editer_acces');



  Route::post('creer_role', 'UtilisateurController@creer_role');
  Route::post('creer_permission', 'UtilisateurController@creer_permis');
  Route::post('assigner_permission', 'UtilisateurController@assigner_permis');
  Route::get('supprimer_permission/{idrol}/{idper}', 'UtilisateurController@supprimer_permis');


  Route::get('form_nouveau_utilisateur', 'UtilisateurController@form_nouveau_utilisateur')->name('users.create')
                                  ->middleware('permissionshinobi:users.create');


  Route::get('form_nouveau_role','UtilisateurController@form_nouveau_role')->name('roles.create')->middleware('permissionshinobi:roles.create');


  Route::get('form_nouveau_permis', 'UtilisateurController@form_nouveau_permis')->name('permis.create')
                                  ->middleware('permissionshinobi:permis.create');


  Route::get('form_editer_utilisateur/{id}', 'UtilisateurController@form_editer_utilisateur');
  Route::get('assigner_role/{idusu}/{idrol}', 'UtilisateurController@assigner_role');
  Route::get('eliminer_role/{idusu}/{idrol}', 'UtilisateurController@eliminer_role');
  Route::get('form_supprimer_utilisateur/{idusu}', 'UtilisateurController@form_supprimer_utilisateur');
  Route::get('supprimer_role/{idrol}', 'UtilisateurController@supprimer_role');

// ******************************************


Route::get('role', 'PageController@role');
Route::get('show', 'PageController@show');

// Nouvelles
Route::resource('nouvelles', 'NouvelleController');


Route::resource('departements', 'DepartementController');
Route::resource('fonctions', 'FonctionController');
Route::resource('participants', 'ParticipantController');
Route::resource('employes', 'EmployeController');
Route::resource('plans', 'PlanController');
Route::resource('plan_travails', 'PlanController');
Route::resource('plan_travail_elements', 'PlanTravailElementController');

// Gestion des acces avec Shinobi pour le modele departement
Route::get('departements', 'DepartementController@index')->name('departement.index')
    ->middleware('permissionshinobi:departement.index');
Route::get('departements/{id}', 'DepartementController@show')->name('departement.show')
    ->middleware('permissionshinobi:departement.show');
Route::get('departements/{id}/edit', 'DepartementController@edit')->name('departement.update')
    ->middleware('permissionshinobi:departement.update');
Route::get('departements/create', 'DepartementController@create')->name('departement.create')
    ->middleware('permissionshinobi:departement.create');

// Gestion des acces avec Shinobi pour le modele fonction
Route::get('fonctions', 'FonctionController@index')->name('fonction.index')
    ->middleware('permissionshinobi:fonction.index');
Route::get('fonctions/{id}', 'FonctionController@show')->name('fonction.show')
    ->middleware('permissionshinobi:fonction.show');
Route::get('fonctions/{id}/edit', 'FonctionController@edit')->name('fonction.update')
    ->middleware('permissionshinobi:fonction.update');
Route::get('fonctions/create', 'FonctionController@create')->name('fonction.create')
    ->middleware('permissionshinobi:fonction.create');

// Gestion des acces avec Shinobi pour le modele participant
Route::get('participants', 'ParticipantController@index')->name('participant.index')
    ->middleware('permissionshinobi:participant.index');
Route::get('participants/{id}', 'ParticipantController@show')->name('participant.show')
    ->middleware('permissionshinobi:participant.show');
Route::get('participants/{id}/edit', 'ParticipantController@edit')->name('participant.update')
    ->middleware('permissionshinobi:participant.update');
Route::get('participants/create', 'ParticipantController@create')->name('participant.create')
    ->middleware('permissionshinobi:participant.create');

// Gestion des acces avec Shinobi pour le modele employe
Route::get('employes', 'EmployeController@index')->name('employe.index')
    ->middleware('permissionshinobi:employe.index');
Route::get('employes/{id}', 'EmployeController@show')->name('employe.show')
    ->middleware('permissionshinobi:employe.show');
Route::get('employes/{id}/edit', 'EmployeController@edit')->name('employe.update')
    ->middleware('permissionshinobi:employe.update');
Route::get('employes/create', 'EmployeController@create')->name('employe.create')
    ->middleware('permissionshinobi:employe.create');

//  Gestion des acces avec Shinobi pour le modele plan de travail
Route::get('plan_travails', 'PlanController@index')->name('plan_travail.index')
    ->middleware('permissionshinobi:plan_travail.index');
Route::get('plan_travails/{id}', 'PlanController@show')->name('plan_travail.show')
    ->middleware('permissionshinobi:plan_travail.show');
Route::get('plan_travails/{id}/edit', 'PlanController@edit')->name('plan_travail.update')
    ->middleware('permissionshinobi:plan_travail.update');
Route::get('plan_travails/create', 'PlanController@create')->name('plan_travail.create')
    ->middleware('permissionshinobi:plan_travail.create');
    Route::get('plan_travails/store', 'PlanController@store')->name('plan_travail.store')
    ->middleware('permissionshinobi:plan_travail.store');    


//  Gestion des acces avec Shinobi pour le modele plan de travail elements
Route::get('plan_travail_elements', 'PlanTravailElementController@index')->name('plan_travail_element.index')
    ->middleware('permissionshinobi:plan_travail_element.index');
Route::get('plan_travail_elements/{id}', 'PlanTravailElementController@show')->name('plan_travail_element.show')
    ->middleware('permissionshinobi:plan_travail_element.show');
Route::get('plan_travail_elements/{id}/edit', 'PlanTravailElementController@edit')->name('plan_travail_element.update')
    ->middleware('permissionshinobi:plan_travail_element.update');
Route::get('plan_travail_elements/create', 'PlanTravailElementController@create')->name('plan_travail_element.create')
    ->middleware('permissionshinobi:plan_travail_element.create');
    Route::get('plan_travail_elements/store', 'PlanTravailElementController@store')->name('plan_travail_element.store')
    ->middleware('permissionshinobi:plan_travail_element.store');      


Route::get('semaines/create/{id}', [
    'as' => 'semaines.create',
    'uses' => 'SemaineController@create'
]);

Route::resource('semaines', 'SemaineController', ['except' => 'create']);
Route::get('/role', 'RoleController@index');

Route::get('/integration/create/{id}', 'ParticipantIntegrationController@create');
Route::post('/integration/store', 'ParticipantIntegrationController@store')->name('integrationParticipant');
Route::get('/integration/edit/{id}', 'ParticipantIntegrationController@edit')->name('participant_integrations');
Route::put('/integration/edit/{id}', 'ParticipantIntegrationController@update')->name('integrations_update');
Route::get('/integration/show/{id}', 'ParticipantIntegrationController@show');

});
