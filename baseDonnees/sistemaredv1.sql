-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 07 mai 2018 à 00:32
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sistemaredv`
--

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_01_20_084450_create_roles_table', 2),
(4, '2015_01_20_084525_create_role_user_table', 2),
(5, '2015_01_24_080208_create_permissions_table', 2),
(6, '2015_01_24_080433_create_permission_role_table', 2),
(7, '2015_12_04_003040_add_special_role_column', 2),
(8, '2017_02_18_194922_update_users', 3);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(18, 'Créer nouveau candidat', 'candidat.create', 'Créer nouveau candidat', '2018-05-06 05:58:34', '2018-05-06 05:58:34'),
(19, 'Éditer candidat', 'candidat.update', 'Éditer un candidat ', '2018-05-06 06:02:06', '2018-05-06 06:02:06'),
(20, 'Lister candidats', 'candidat.show', 'Lister candidats', '2018-05-06 06:03:06', '2018-05-06 06:03:06'),
(21, 'Éliminer  candidate', 'candidat.delete', 'Éliminer un candidat', '2018-05-06 06:05:38', '2018-05-06 06:05:38'),
(22, 'Créer nouveau participant', 'participant.create', 'crée un nouveau participant', '2018-05-06 06:10:08', '2018-05-06 06:10:08'),
(23, 'Éditer participant', 'participant.update', 'Édite un participant', '2018-05-06 06:11:44', '2018-05-06 06:11:44'),
(24, 'Lister participants', 'participant.show', 'Liste tous les participant', '2018-05-06 06:13:37', '2018-05-06 06:13:37'),
(25, 'Éliminer un participant', 'participant.delete', 'Élimine un participant', '2018-05-06 06:14:40', '2018-05-06 06:14:40'),
(26, 'Créer nouvel employé', 'employe.create', 'Crée un nouvel employé', '2018-05-06 06:16:47', '2018-05-06 06:16:47'),
(27, 'Éditer employé', 'employe.update', 'Édite un employé', '2018-05-06 06:17:52', '2018-05-06 06:17:52'),
(28, 'Lister employés', 'employe.show', 'Liste à tous les employés', '2018-05-06 06:19:29', '2018-05-06 06:19:29'),
(29, 'Éliminer employé', 'employe.delete', 'Élimine un employé', '2018-05-06 06:20:24', '2018-05-06 06:20:24'),
(30, 'Créer  article', 'article.create', 'Crée un article', '2018-05-06 06:21:34', '2018-05-06 06:21:34'),
(31, 'Éditer article', 'article.update', 'Édite un article', '2018-05-06 06:22:11', '2018-05-06 06:22:11'),
(32, 'Lister articles', 'article.show', 'liste tous les article', '2018-05-06 06:23:12', '2018-05-06 06:23:12'),
(33, 'Éliminer article', 'article.delete', 'Élimine un article', '2018-05-06 06:23:47', '2018-05-06 06:23:47');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(44, 26, 19, '2018-05-06 06:25:04', '2018-05-06 06:25:04'),
(45, 27, 19, '2018-05-06 06:25:16', '2018-05-06 06:25:16'),
(46, 28, 19, '2018-05-06 06:25:38', '2018-05-06 06:25:38'),
(47, 29, 19, '2018-05-06 06:25:55', '2018-05-06 06:25:55'),
(48, 22, 20, '2018-05-06 06:26:40', '2018-05-06 06:26:40'),
(51, 23, 20, '2018-05-06 06:28:26', '2018-05-06 06:28:26'),
(52, 24, 20, '2018-05-06 06:29:05', '2018-05-06 06:29:05'),
(53, 25, 20, '2018-05-06 06:29:30', '2018-05-06 06:29:30'),
(54, 30, 20, '2018-05-06 06:30:03', '2018-05-06 06:30:03'),
(55, 31, 20, '2018-05-06 06:30:17', '2018-05-06 06:30:17'),
(56, 32, 20, '2018-05-06 06:30:39', '2018-05-06 06:30:39'),
(57, 33, 20, '2018-05-06 06:30:49', '2018-05-06 06:30:49'),
(58, 23, 22, '2018-05-06 06:31:35', '2018-05-06 06:31:35'),
(59, 24, 22, '2018-05-06 06:31:53', '2018-05-06 06:31:53'),
(60, 27, 23, '2018-05-06 06:32:48', '2018-05-06 06:32:48'),
(62, 28, 23, '2018-05-06 06:33:54', '2018-05-06 06:33:54'),
(63, 32, 23, '2018-05-06 06:34:16', '2018-05-06 06:34:16'),
(64, 24, 23, '2018-05-06 06:34:53', '2018-05-06 06:34:53');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`, `special`) VALUES
(1, 'ADMINISTRATEUR', 'administrateur', 'il sera l\'administrateur du système', '2017-03-06 03:44:12', '2017-03-06 03:44:12', 'all-access'),
(19, 'CHEF RH', 'chef RH', 'Chef de ressource humaine', '2018-05-06 05:42:04', '2018-05-06 05:42:04', NULL),
(20, 'COORDINATRICE T.I', 'coordinatrice T.I', 'Coordinatrice T.I', '2018-05-06 05:45:08', '2018-05-06 05:45:08', NULL),
(21, 'COORDINATEUR T.I', 'coordinateur T.I', 'Coordinateur T.I', '2018-05-06 05:46:35', '2018-05-06 05:46:35', NULL),
(22, 'PARTICIPANT', 'participant', 'Participant', '2018-05-06 05:48:09', '2018-05-06 05:48:09', NULL),
(23, 'EMPLOYÉ', 'employé', 'Employé', '2018-05-06 05:48:55', '2018-05-06 05:48:55', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(8, 1, 5, '2018-04-19 18:12:49', '2018-04-19 18:12:49'),
(19, 1, 27, '2018-05-06 05:27:09', '2018-05-06 05:27:09'),
(20, 20, 28, '2018-05-06 06:40:47', '2018-05-06 06:40:47'),
(21, 22, 29, '2018-05-06 06:41:11', '2018-05-06 06:41:11'),
(22, 19, 30, '2018-05-06 06:41:39', '2018-05-06 06:41:39'),
(23, 21, 31, '2018-05-06 06:42:05', '2018-05-06 06:42:05');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prenoms` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `noms` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `prenoms`, `noms`, `telephone`) VALUES
(5, 'ADMINISTRATEUR ADMIN', 'admin@hotmail.com', '$2y$10$vzvYd/DuH3tbJy7a/cCeVeUrMdFGFY6UYNDRUyQd/5SC0.33VfLxe', 'lUHmiy21pFQ1lts5XU9OZQ1OMzr1aLmEzwnt9kVvk7SEuvzQqc0NXi57K91Y', '2018-04-19 18:09:26', '2018-05-06 05:27:44', 'ADMINISTRATEUR', 'ADMIN', '4189536811'),
(27, 'LARRY  NAVA', 'larry@hotmail.com', '$2y$10$qvHdx48Jnr/zPEcY9is6GuWqPkndjRkTQWpdMkC5kO0HuEAR9KG7y', 'fkGtYdnFS1CmSp7itzByFq1pF44HMyr6gqj2b1gJeGmflK0T8xfJxXnYhaIW', '2018-05-06 05:26:34', '2018-05-06 07:01:08', 'LARRY ', 'NAVA', '4186934521'),
(28, 'ZOE BOUCHARD', 'Zoe@hotmail.com', '$2y$10$VLj2LHpe/9Eef17qqbnDbOZaAyn5zdWXaCAn1DKzKy2Px97I2NSp.', '1efghTADiJ5yjJHtQ6BoBpllNaeFTtslPoQmOxKWt0WhdYuVjVMBJsNGAHtX', '2018-05-06 06:37:19', '2018-05-06 06:44:53', 'ZOE', 'BOUCHARD', '4185632147'),
(29, 'JOSEPH TREMBLAY', 'joseph@hotmail.com', '$2y$10$doptDprFM4J0vhscZzpWrehKFQGOMWNnf.tzT4VkwEMjAN75yqyyS', 'Xm1PYzt5mkSOzctGM8cCwGoPrB1S4P1Jx56LEvEwbbWkcZtPFc08PGRVmjt5', '2018-05-06 06:38:19', '2018-05-06 06:46:09', 'JOSEPH', 'TREMBLAY', '4187534125'),
(30, 'ALICE PELLETIER', 'alice@hotmail.com', '$2y$10$dXJbC3KhHWtREHk9F2.6eeM0VnyEYh1NJXhyIviCK8iJKg2YaDLW.', 'hzA6pRb4wYk8yNIaEWvbYlUx5RNGOvbvQgLoGNuQZobnJ8vucPNDk5xdPrw7', '2018-05-06 06:39:14', '2018-05-06 06:43:51', 'ALICE', 'PELLETIER', '4189516321'),
(31, 'PIERRE GAGNON', 'pierre@hotmail.com', '$2y$10$FTZRQJS7h8lysd6Hz235PeFrZSkMmJX5Rfr9QmEpEGK4HKjJVAjhS', 'YDhWoDj4GVBMiN0lahjvyDr4MIB7ZtnTgYKWLdb46kBbG8Llg5ZDkEQmg48R', '2018-05-06 06:40:03', '2018-05-06 06:50:40', 'PIERRE', 'GAGNON', '4181236589');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
