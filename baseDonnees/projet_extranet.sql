-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 07 mai 2019 à 21:37
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet_extranet`
--

-- --------------------------------------------------------

--
-- Structure de la table `ajout_image_articles`
--

CREATE TABLE `ajout_image_articles` (
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `employe_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ateliers`
--

CREATE TABLE `ateliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_atelier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_atelier` text COLLATE utf8_unicode_ci NOT NULL,
  `date_atelier` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participantAtelier_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `coordonnees`
--

CREATE TABLE `coordonnees` (
  `id` int(10) UNSIGNED NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lieu_naissance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `adresse` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `appartement` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `coordonnees`
--

INSERT INTO `coordonnees` (`id`, `prenom`, `nom`, `sexe`, `lieu_naissance`, `date_naissance`, `adresse`, `appartement`, `ville`, `province`, `code_postal`, `telephone`, `created_at`, `updated_at`) VALUES
(1, 'Denis', 'Poisson', 'homme', 'Deschambault', '1970-07-23', '107 Mathieu', '0', 'Deschambault', 'Québec', 'G0A1S0', '14188500444', '2019-04-24 21:25:55', '2019-05-02 23:27:01'),
(2, 'Denis', 'Poisson', 'homme', 'Deschambault', '2013-10-29', '107 Mathieu', '0', 'Deschambault', 'Québec', 'G0A1S0', '14183802449', '2019-04-24 21:27:24', '2019-04-25 20:45:36'),
(3, 'Denis', 'Poisson', 'homme', 'Deschambault', '2017-05-01', '107 Mathieu', '0', 'québec', 'Québec', 'G1R2G1', '14188500444', '2019-04-25 23:05:19', '2019-04-25 23:05:19'),
(4, 'Milan', 'Kundera', 'homme', 'Prague', '1960-05-03', '1 rue ', '0', 'Lévis', 'Québec', 'G0A1S0', '4188881234', '2019-04-25 23:42:05', '2019-04-25 23:42:05'),
(5, 'Employé', 'Modèle', 'nonbinaire', 'Inconnu', '1980-12-31', '100 rue des bouleaux', '0', 'Québec', 'Québec', 'G0A1S0', '1231231234', '2019-05-02 23:29:35', '2019-05-02 23:29:35'),
(6, 'Employé', 'Modèle', 'nonbinaire', 'Inconnu', '1980-12-31', '100 rue des bouleaux', '0', 'Québec', 'Québec', 'G0A1S0', '1231231234', '2019-05-02 23:34:10', '2019-05-02 23:34:10'),
(7, 'Employé', 'SansAccent', 'nonbinaire', 'Inconnu', '1980-12-31', '100 rue des bouleaux', '0', 'Québec', 'Québec', 'G0A1S0', '1231231234', '2019-05-02 23:35:05', '2019-05-02 23:35:05'),
(8, 'Employé', 'Modèle', 'nonbinaire', 'Ìnconnu', '1990-11-11', '1 rue des bouleaux', '0', 'québec', 'Québec', 'G0A1S0', '1231231234', '2019-05-03 00:06:15', '2019-05-03 00:06:15'),
(9, 'Employé', 'Modèle', 'nonbinaire', 'Ìnconnu', '1990-11-11', '1 rue des bouleaux', '0', 'québec', 'Québec', 'G0A1S0', '1231231234', '2019-05-03 00:26:01', '2019-05-03 00:26:01'),
(10, 'Pierre', 'Labrie', 'homme', 'Inconnu', '2008-12-30', '1 rue', '1', 'Deschambault', 'Québec', 'G0A1S0', '1231231234', '2019-05-03 17:43:54', '2019-05-03 17:43:54');

-- --------------------------------------------------------

--
-- Structure de la table `departements`
--

CREATE TABLE `departements` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_departement` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nombre_semaine_integration` int(11) NOT NULL,
  `max_semaine_prolongation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `departements`
--

INSERT INTO `departements` (`id`, `nom_departement`, `created_at`, `updated_at`, `nombre_semaine_integration`, `max_semaine_prolongation`) VALUES
(1, 'Administration', '2018-05-17 18:54:58', '2018-05-17 18:54:58', 15, 2),
(2, 'Technologie de l\'Information', '2019-04-24 21:15:44', '2019-04-24 21:15:44', 18, 2),
(3, 'Tous les départements', '2019-04-24 23:29:36', '2019-04-24 23:29:36', 15, 2),
(4, 'Bureautique', '2019-04-24 23:32:11', '2019-04-24 23:32:11', 15, 2),
(5, 'Communication et marketting', '2019-05-03 17:40:56', '2019-05-03 17:40:56', 15, 2),
(6, 'Communication et marketting', '2019-05-06 18:52:05', '2019-05-06 18:52:05', 15, 2);

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `id` int(10) UNSIGNED NOT NULL,
  `isAdmin` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `departement_id` int(10) UNSIGNED NOT NULL,
  `fonction_id` int(10) UNSIGNED NOT NULL,
  `coordonnees_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `employes`
--

INSERT INTO `employes` (`id`, `isAdmin`, `created_at`, `updated_at`, `departement_id`, `fonction_id`, `coordonnees_id`) VALUES
(1, 0, '2019-04-24 21:25:55', '2019-04-24 21:25:55', 2, 2, 1),
(2, 0, '2019-05-02 23:29:35', '2019-05-02 23:29:35', 4, 2, 5),
(3, 0, '2019-05-02 23:34:10', '2019-05-02 23:34:10', 4, 2, 6),
(4, 0, '2019-05-02 23:35:05', '2019-05-02 23:35:05', 4, 2, 7),
(5, 0, '2019-05-03 00:06:15', '2019-05-03 00:06:15', 4, 1, 8),
(6, 0, '2019-05-03 00:26:01', '2019-05-03 00:26:01', 4, 1, 9);

-- --------------------------------------------------------

--
-- Structure de la table `fonctions`
--

CREATE TABLE `fonctions` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_fonction` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fonctions`
--

INSERT INTO `fonctions` (`id`, `nom_fonction`, `created_at`, `updated_at`) VALUES
(1, 'participant', '2018-05-17 22:28:57', '2018-05-17 22:28:57'),
(2, 'Employé', '2019-04-24 21:18:48', '2019-04-24 21:18:48'),
(3, 'Responsable TI', '2019-05-03 17:41:27', '2019-05-03 17:41:27');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_01_20_084450_create_roles_table', 2),
(4, '2015_01_20_084525_create_role_user_table', 2),
(5, '2015_01_24_080208_create_permissions_table', 2),
(6, '2015_01_24_080433_create_permission_role_table', 2),
(7, '2015_12_04_003040_add_special_role_column', 2),
(8, '2017_02_18_194922_update_users', 3),
(9, '2018_05_16_182115_create_coordonnees_table', 4),
(10, '2018_05_16_182224_create_participants_table', 4),
(11, '2018_05_16_182329_create_departements_table', 4),
(12, '2018_05_16_182538_create_fonctions_table', 4),
(13, '2018_05_16_182648_create_employes_table', 4),
(14, '2018_05_16_182722_create_articles_table', 4),
(15, '2018_05_16_182914_create_participant_integrations_table', 4),
(16, '2018_05_16_183102_create_plan_travail_sections_table', 4),
(17, '2018_05_16_183217_create_plan_travail_elements_table', 4),
(18, '2018_05_16_183317_create_plan_travails_table', 4),
(19, '2018_05_16_183431_create_ateliers_table', 4),
(20, '2018_05_16_183602_create_suivis_table', 4),
(21, '2018_05_16_183852_create_roles1s_table', 4),
(22, '2018_05_16_184004_create_ajout_image_articles_table', 4),
(23, '2018_05_16_184114_create_participant_emplois_table', 4),
(24, '2018_05_16_184202_create_semaine_plans_table', 4),
(25, '2018_05_16_184707_create_plan_travail_element_semaine_plan', 4),
(26, '2017_10_17_170735_create_permission_user_table', 5),
(27, '2019_04_18_133932_create_plan_travail_element_participants_table', 5),
(28, '2019_04_18_160436_add_participant_element_id_to_plan_travail_element_semaine_plan', 5),
(29, '2019_04_18_172809_add_coordonnees_id_to_participants', 5),
(30, '2019_04_24_174522_add_nombre_max_semaine_to_departements_table', 6);

-- --------------------------------------------------------

--
-- Structure de la table `participants`
--

CREATE TABLE `participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `niveau_etudes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domaine_etudes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admis_personne` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `coordonnees_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `participants`
--

INSERT INTO `participants` (`id`, `niveau_etudes`, `domaine_etudes`, `admis_personne`, `created_at`, `updated_at`, `coordonnees_id`) VALUES
(1, '', '', 1, '2019-04-24 21:27:24', '2019-04-25 20:53:15', 2),
(2, '', '', 0, '2019-04-25 23:05:19', '2019-04-25 23:05:19', 3),
(3, '', '', 1, '2019-04-25 23:42:05', '2019-04-25 23:46:19', 4),
(4, '', 'Informatique', 1, '2019-05-03 17:43:54', '2019-05-03 17:47:53', 10);

-- --------------------------------------------------------

--
-- Structure de la table `participant_emplois`
--

CREATE TABLE `participant_emplois` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titre_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entreprise_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `personne_ressource_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `courriel_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suite_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ville_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal_emploi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `site_web_emploi` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `date_debut_emploi` date NOT NULL,
  `commentaires_emploi` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participantEmploi_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `participant_integrations`
--

CREATE TABLE `participant_integrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `numero_participant` int(11) NOT NULL,
  `urgence_contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urgence_coordonnees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `periode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_debut_stage` date NOT NULL,
  `nombre_semaine_prolongation` int(11) NOT NULL,
  `nombre_semaine_absence` int(11) NOT NULL,
  `nombre_semaine_totale` int(11) NOT NULL,
  `date_fin_prevue_stage` date NOT NULL,
  `date_fin_reelle_stage` date NOT NULL,
  `reference` text COLLATE utf8_unicode_ci NOT NULL,
  `clientele` text COLLATE utf8_unicode_ci NOT NULL,
  `source_revenu` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `financement` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `no_dossier_emploiQC` int(11) NOT NULL,
  `nom_agent` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `adresse_agent` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `provenance` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `particularite` text COLLATE utf8_unicode_ci NOT NULL,
  `transport` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `commentaires` text COLLATE utf8_unicode_ci NOT NULL,
  `raison_depart` text COLLATE utf8_unicode_ci NOT NULL,
  `en_emploi` tinyint(1) NOT NULL,
  `objectif_atteint` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participant_id` int(10) UNSIGNED NOT NULL,
  `departementIntegration_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `participant_integrations`
--

INSERT INTO `participant_integrations` (`id`, `photo`, `numero_participant`, `urgence_contact`, `urgence_coordonnees`, `periode`, `date_debut_stage`, `nombre_semaine_prolongation`, `nombre_semaine_absence`, `nombre_semaine_totale`, `date_fin_prevue_stage`, `date_fin_reelle_stage`, `reference`, `clientele`, `source_revenu`, `financement`, `no_dossier_emploiQC`, `nom_agent`, `adresse_agent`, `provenance`, `particularite`, `transport`, `commentaires`, `raison_depart`, `en_emploi`, `objectif_atteint`, `created_at`, `updated_at`, `participant_id`, `departementIntegration_id`) VALUES
(1, 'denispoisson.JPG', 1, 'Denis Poisson', '4188888888', 'jour', '2019-01-01', 2, 1, 18, '2019-05-01', '2019-05-04', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 1234, 'Denis Poisson', '107 Mathieu', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 0, 0, '2019-04-25 20:53:15', '2019-04-25 20:53:15', 1, 1),
(3, 'flowersky.jpg', 2, 'Maman', '4188888888', 'jour', '2019-04-26', 1, 1, 15, '2019-08-01', '2019-08-02', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 5678, 'Shany', 'Donnacona', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 0, 0, '2019-04-25 23:46:19', '2019-05-02 22:50:51', 3, 1),
(4, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-31', 2, 1, 15, '2019-05-31', '2019-12-31', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', 'Paris', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 17:47:53', '2019-05-03 17:47:53', 4, 1),
(5, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-31', 2, 1, 15, '2019-05-31', '2019-12-31', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', 'Paris', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 17:59:32', '2019-05-03 17:59:32', 4, 1),
(6, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-31', 2, 1, 15, '2019-05-31', '2019-12-31', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', 'Paris', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 18:00:58', '2019-05-03 18:00:58', 4, 1),
(7, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-31', 2, 1, 15, '2019-05-31', '2019-12-31', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', 'Paris', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 18:06:01', '2019-05-03 18:06:01', 4, 1),
(8, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-01', 2, 1, 15, '2019-05-01', '2019-05-26', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', '107 Mathieu', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 18:08:49', '2019-05-03 18:08:49', 4, 1),
(9, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-01', 2, 1, 15, '2019-05-01', '2019-05-26', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', '107 Mathieu', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 18:37:19', '2019-05-03 18:37:19', 4, 1),
(10, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-01', 2, 1, 15, '2019-05-01', '2019-05-26', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', '107 Mathieu', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 18:43:52', '2019-05-03 18:43:52', 4, 1),
(11, 'man.jpg', 3, 'maman', '4188888888', 'jour', '2019-01-01', 2, 1, 15, '2019-05-01', '2019-05-26', 'emploi québec', 'sans revenu', 'emploi québec', 'emploi québec', 56789, 'Denis Poisson', '107 Mathieu', 'emploiquebec', 'aucune', 'oui', 'aucun', 'pas de raison', 1, 1, '2019-05-03 19:51:36', '2019-05-03 19:51:36', 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(18, 'Créer nouveau candidat', 'candidat.create', 'Créer nouveau candidat', '2018-05-06 09:58:34', '2018-05-06 09:58:34'),
(19, 'Éditer candidat', 'candidat.update', 'Éditer un candidat ', '2018-05-06 10:02:06', '2018-05-06 10:02:06'),
(20, 'Lister candidats', 'candidat.show', 'Lister candidats', '2018-05-06 10:03:06', '2018-05-06 10:03:06'),
(21, 'Éliminer  candidate', 'candidat.delete', 'Éliminer un candidat', '2018-05-06 10:05:38', '2018-05-06 10:05:38'),
(22, 'Créer nouveau participant', 'participant.create', 'crée un nouveau participant', '2018-05-06 10:10:08', '2018-05-06 10:10:08'),
(23, 'Éditer participant', 'participant.update', 'Édite un participant', '2018-05-06 10:11:44', '2018-05-06 10:11:44'),
(24, 'Lister participants', 'participant.show', 'Liste tous les participant', '2018-05-06 10:13:37', '2018-05-06 10:13:37'),
(25, 'Éliminer un participant', 'participant.delete', 'Élimine un participant', '2018-05-06 10:14:40', '2018-05-06 10:14:40'),
(26, 'Créer nouvel employé', 'employe.create', 'Crée un nouvel employé', '2018-05-06 10:16:47', '2018-05-06 10:16:47'),
(27, 'Éditer employé', 'employe.update', 'Édite un employé', '2018-05-06 10:17:52', '2018-05-06 10:17:52'),
(28, 'Lister employés', 'employe.show', 'Liste à tous les employés', '2018-05-06 10:19:29', '2018-05-06 10:19:29'),
(29, 'Éliminer employé', 'employe.delete', 'Élimine un employé', '2018-05-06 10:20:24', '2018-05-06 10:20:24'),
(30, 'Créer  article', 'article.create', 'Crée un article', '2018-05-06 10:21:34', '2018-05-06 10:21:34'),
(31, 'Éditer article', 'article.update', 'Édite un article', '2018-05-06 10:22:11', '2018-05-06 10:22:11'),
(32, 'Lister articles', 'article.show', 'liste tous les article', '2018-05-06 10:23:12', '2018-05-06 10:23:12'),
(33, 'Éliminer article', 'article.delete', 'Élimine un article', '2018-05-06 10:23:47', '2018-05-06 10:23:47'),
(34, 'Voir liste d\'employés', 'employe.index', 'Affiche tout les employés', '2018-05-18 20:08:49', '2018-05-18 20:08:49'),
(35, 'Voir liste des participants', 'participant.index', 'Affiche tous les participants', '2019-05-07 18:18:41', '2019-05-07 18:18:41'),
(36, 'Créer nouveau département', 'departement.create', 'Créer un nouveau département', '2019-05-07 18:38:53', '2019-05-07 18:38:53'),
(37, 'Éditer département', 'departement.update', 'Éditer un département', '2019-05-07 18:40:01', '2019-05-07 18:40:01'),
(38, 'Voir liste des départements', 'departement.index', 'Lister tous les départements', '2019-05-07 18:40:47', '2019-05-07 18:40:47'),
(41, 'Voir un département', 'departement.show', 'Consulter un département', '2019-05-07 18:44:25', '2019-05-07 18:44:25'),
(42, 'Éliminer département', 'departement.delete', 'Éliminer un département', '2019-05-07 19:57:09', '2019-05-07 19:57:09'),
(43, 'Voir fonction', 'fonction.show', 'Voir une fonction', '2019-05-07 19:59:26', '2019-05-07 19:59:26'),
(44, 'Lister fonctions', 'fonction.index', 'Lister toutes les fonctions', '2019-05-07 20:00:02', '2019-05-07 20:00:02'),
(45, 'Éditer fonction', 'fonction.update', 'Éditer une fonction', '2019-05-07 20:00:40', '2019-05-07 20:00:40'),
(46, 'Créer nouvelle fonction', 'fonction.create', 'Créer une nouvelle fonction', '2019-05-07 20:01:17', '2019-05-07 20:01:17'),
(47, 'Éliminer fonction', 'fonction.delete', 'Éliminer une fonction', '2019-05-07 20:01:52', '2019-05-07 20:01:52'),
(48, 'Voir liste plans de travail', 'plan_travail.index', 'Voir liste des plans de travail', '2019-05-07 21:35:15', '2019-05-07 21:35:15'),
(49, 'Voir plan de travail', 'plan_travail.show', 'Voir un plan de travail', '2019-05-07 21:35:53', '2019-05-07 21:35:53'),
(50, 'Éditer plans de travail', 'plan_travail.update', 'Éditer les plans de travail', '2019-05-07 21:36:25', '2019-05-07 21:36:25'),
(51, 'Créer nouveau plan de travail', 'plan_travail.create', 'Créer un nouveau plan de travail', '2019-05-07 21:36:56', '2019-05-07 21:36:56'),
(52, 'Créer plan de travail', 'plan_travail.store', 'Créer un plan de travail', '2019-05-07 21:47:25', '2019-05-07 21:47:25'),
(53, 'Lister éléments du plan de travail', 'plan_travail_element.index', 'Lister tous les éléments du plan de travail', '2019-05-07 22:06:36', '2019-05-07 22:06:36'),
(54, 'Voir élément du plan de travail', 'plan_travail_element.show', 'Voir un élément du plan de travail', '2019-05-07 22:07:26', '2019-05-07 22:07:26'),
(55, 'Éditer élément du plan de travail', 'plan_travail_element.update', 'Éditer un élément du plan de travail', '2019-05-07 22:08:06', '2019-05-07 22:08:06'),
(56, 'Créer élément du plan de travail', 'plan_travail_element.create', 'Créer un élément du plan de travail', '2019-05-07 22:08:49', '2019-05-07 22:08:49'),
(57, 'Créer nouvel élément du plan de travail', 'plan_travail_element.store', 'Créer un nouvel élément du plan de travail', '2019-05-07 22:09:34', '2019-05-07 22:09:34'),
(58, 'Éliminer élément du plan de travail', 'plan_travail_element.delete', 'Éliminer un élément du plan de travail', '2019-05-07 22:10:09', '2019-05-07 22:10:09'),
(59, 'Voir la liste des utilisateurs', 'users.index', 'Voir la liste de tous les utilisateurs', '2019-05-07 22:24:58', '2019-05-07 22:24:58');

-- --------------------------------------------------------

--
-- Structure de la table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(44, 26, 19, '2018-05-06 10:25:04', '2018-05-06 10:25:04'),
(45, 27, 19, '2018-05-06 10:25:16', '2018-05-06 10:25:16'),
(46, 28, 19, '2018-05-06 10:25:38', '2018-05-06 10:25:38'),
(47, 29, 19, '2018-05-06 10:25:55', '2018-05-06 10:25:55'),
(48, 22, 20, '2018-05-06 10:26:40', '2018-05-06 10:26:40'),
(51, 23, 20, '2018-05-06 10:28:26', '2018-05-06 10:28:26'),
(52, 24, 20, '2018-05-06 10:29:05', '2018-05-06 10:29:05'),
(53, 25, 20, '2018-05-06 10:29:30', '2018-05-06 10:29:30'),
(54, 30, 20, '2018-05-06 10:30:03', '2018-05-06 10:30:03'),
(55, 31, 20, '2018-05-06 10:30:17', '2018-05-06 10:30:17'),
(56, 32, 20, '2018-05-06 10:30:39', '2018-05-06 10:30:39'),
(57, 33, 20, '2018-05-06 10:30:49', '2018-05-06 10:30:49'),
(58, 23, 22, '2018-05-06 10:31:35', '2018-05-06 10:31:35'),
(59, 24, 22, '2018-05-06 10:31:53', '2018-05-06 10:31:53'),
(60, 27, 23, '2018-05-06 10:32:48', '2018-05-06 10:32:48'),
(62, 28, 23, '2018-05-06 10:33:54', '2018-05-06 10:33:54'),
(63, 32, 23, '2018-05-06 10:34:16', '2018-05-06 10:34:16'),
(64, 24, 23, '2018-05-06 10:34:53', '2018-05-06 10:34:53'),
(65, 34, 23, '2019-05-07 17:39:38', '2019-05-07 17:39:38'),
(66, 34, 19, '2019-05-07 17:59:52', '2019-05-07 17:59:52'),
(67, 35, 20, '2019-05-07 18:18:54', '2019-05-07 18:18:54'),
(68, 36, 19, '2019-05-07 18:42:32', '2019-05-07 18:42:32'),
(69, 37, 19, '2019-05-07 18:42:43', '2019-05-07 18:42:43'),
(70, 38, 19, '2019-05-07 18:42:53', '2019-05-07 18:42:53'),
(71, 41, 19, '2019-05-07 18:44:39', '2019-05-07 18:44:39'),
(72, 42, 19, '2019-05-07 19:57:30', '2019-05-07 19:57:30'),
(73, 43, 19, '2019-05-07 20:02:10', '2019-05-07 20:02:10'),
(74, 44, 19, '2019-05-07 20:02:21', '2019-05-07 20:02:21'),
(75, 45, 19, '2019-05-07 20:02:31', '2019-05-07 20:02:31'),
(76, 46, 19, '2019-05-07 20:02:39', '2019-05-07 20:02:39'),
(77, 47, 19, '2019-05-07 20:02:50', '2019-05-07 20:02:50'),
(78, 48, 20, '2019-05-07 21:37:12', '2019-05-07 21:37:12'),
(79, 49, 20, '2019-05-07 21:37:22', '2019-05-07 21:37:22'),
(80, 50, 20, '2019-05-07 21:37:34', '2019-05-07 21:37:34'),
(81, 51, 20, '2019-05-07 21:37:41', '2019-05-07 21:37:41'),
(82, 52, 20, '2019-05-07 21:47:40', '2019-05-07 21:47:40'),
(83, 49, 22, '2019-05-07 21:52:39', '2019-05-07 21:52:39'),
(84, 53, 20, '2019-05-07 22:11:33', '2019-05-07 22:11:33'),
(85, 54, 20, '2019-05-07 22:11:43', '2019-05-07 22:11:43'),
(86, 55, 20, '2019-05-07 22:11:54', '2019-05-07 22:11:54'),
(87, 56, 20, '2019-05-07 22:12:07', '2019-05-07 22:12:07'),
(88, 57, 20, '2019-05-07 22:12:36', '2019-05-07 22:12:36'),
(89, 58, 20, '2019-05-07 22:12:46', '2019-05-07 22:12:46'),
(90, 59, 20, '2019-05-07 22:25:06', '2019-05-07 22:25:06'),
(91, 59, 19, '2019-05-07 22:25:21', '2019-05-07 22:25:21');

-- --------------------------------------------------------

--
-- Structure de la table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `plan_travails`
--

CREATE TABLE `plan_travails` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participant_id` int(10) UNSIGNED NOT NULL,
  `element_id` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `plan_travails`
--

INSERT INTO `plan_travails` (`id`, `created_at`, `updated_at`, `participant_id`, `element_id`) VALUES
(1, '2019-04-25 22:34:55', '2019-04-25 23:59:36', 1, 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"5\";i:5;s:1:\"6\";}'),
(2, '2019-04-25 23:02:39', '2019-04-25 23:02:39', 1, 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"2\";i:3;s:1:\"3\";}'),
(3, '2019-04-25 23:23:34', '2019-05-02 22:41:13', 2, 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"5\";i:5;s:1:\"6\";}'),
(4, '2019-04-25 23:55:11', '2019-05-03 19:57:21', 3, 'a:7:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"5\";i:5;s:1:\"6\";i:6;s:1:\"7\";}'),
(5, '2019-05-03 19:56:20', '2019-05-03 19:56:20', 4, 'a:7:{i:0;s:1:\"1\";i:1;s:1:\"4\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"5\";i:5;s:1:\"6\";i:6;s:1:\"7\";}');

-- --------------------------------------------------------

--
-- Structure de la table `plan_travail_elements`
--

CREATE TABLE `plan_travail_elements` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_elements` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_element` text COLLATE utf8_unicode_ci NOT NULL,
  `actif_element` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `departement_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `plan_travail_elements`
--

INSERT INTO `plan_travail_elements` (`id`, `nom_elements`, `description_element`, `actif_element`, `created_at`, `updated_at`, `departement_id`, `section_id`) VALUES
(1, 'HTML', 'Langage HTML', 0, '2019-04-24 21:22:27', '2019-04-24 23:05:36', 2, 1),
(2, 'Twitter', 'Atelier sur Twitter', 1, '2019-04-24 21:24:27', '2019-04-24 21:24:27', 2, 2),
(3, 'Facebook', 'Atelier sur facebook', 1, '2019-04-24 23:15:25', '2019-04-24 23:15:25', 1, 2),
(4, 'CSS', 'Langage CSS', 1, '2019-04-25 00:09:22', '2019-04-25 00:09:22', 2, 1),
(5, 'Laravel', 'Framework Laravel', 1, '2019-04-25 23:54:24', '2019-04-25 23:54:24', 2, 3),
(6, 'Composer', 'Composer', 1, '2019-04-25 23:54:54', '2019-04-25 23:54:54', 2, 3),
(7, 'Atelier Anglais', 'Atelier Anglais', 0, '2019-05-02 22:57:18', '2019-05-02 23:02:16', 4, 3),
(8, 'Joomla', 'CMS JOOMLA', 0, '2019-05-07 22:18:02', '2019-05-07 22:18:20', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `plan_travail_element_participants`
--

CREATE TABLE `plan_travail_element_participants` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `element_id` int(10) UNSIGNED NOT NULL,
  `participant_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `plan_travail_element_semaine_plan`
--

CREATE TABLE `plan_travail_element_semaine_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `element_id` int(10) UNSIGNED NOT NULL,
  `semaine_plan_id` int(10) UNSIGNED NOT NULL,
  `pourcentage` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `plan_travail_element_semaine_plan`
--

INSERT INTO `plan_travail_element_semaine_plan` (`id`, `element_id`, `semaine_plan_id`, `pourcentage`, `created_at`, `updated_at`) VALUES
(1, 2, 4, '15.00', '2019-05-02 22:40:56', '2019-05-02 22:40:56'),
(2, 3, 4, '10.00', '2019-05-02 22:40:56', '2019-05-02 22:40:56'),
(3, 1, 4, '0.00', '2019-05-02 22:40:56', '2019-05-02 22:40:56'),
(4, 4, 4, '0.00', '2019-05-02 22:40:56', '2019-05-02 22:40:56'),
(5, 2, 5, '50.00', '2019-05-02 22:44:34', '2019-05-02 22:44:34'),
(6, 3, 5, '50.00', '2019-05-02 22:44:34', '2019-05-02 22:44:34'),
(7, 1, 5, '0.00', '2019-05-02 22:44:34', '2019-05-02 22:44:34'),
(8, 4, 5, '0.00', '2019-05-02 22:44:34', '2019-05-02 22:44:34'),
(9, 2, 6, '15.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(10, 3, 6, '85.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(11, 1, 6, '0.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(12, 4, 6, '0.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(13, 5, 6, '0.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(14, 6, 6, '0.00', '2019-05-02 22:48:30', '2019-05-02 22:48:30'),
(15, 2, 7, '20.00', '2019-05-03 17:39:03', '2019-05-03 17:39:03'),
(16, 3, 7, '10.00', '2019-05-03 17:39:03', '2019-05-03 17:39:03'),
(17, 1, 7, '0.00', '2019-05-03 17:39:03', '2019-05-03 17:39:03'),
(18, 4, 7, '0.00', '2019-05-03 17:39:03', '2019-05-03 17:39:03'),
(19, 5, 7, '0.00', '2019-05-03 17:39:03', '2019-05-03 17:39:03'),
(20, 2, 8, '20.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(21, 3, 8, '15.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(22, 1, 8, '0.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(23, 4, 8, '0.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(24, 5, 8, '0.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(25, 6, 8, '0.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52'),
(26, 7, 8, '0.00', '2019-05-03 19:56:52', '2019-05-03 19:56:52');

-- --------------------------------------------------------

--
-- Structure de la table `plan_travail_sections`
--

CREATE TABLE `plan_travail_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_section` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_section` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `plan_travail_sections`
--

INSERT INTO `plan_travail_sections` (`id`, `nom_section`, `description_section`, `created_at`, `updated_at`) VALUES
(1, 'Intégration', 'Activités d\'intégration des participants', '2019-04-24 04:00:00', '2019-04-24 04:00:00'),
(2, 'Competences', 'Competences', '2019-04-24 04:00:00', '2019-04-24 04:00:00'),
(3, 'POLYVALENCE', 'section polyvalence', '2019-04-25 04:00:00', '2019-04-25 04:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`, `special`) VALUES
(1, 'ADMINISTRATEUR', 'administrateur', 'il sera l\'administrateur du système', '2017-03-06 03:44:12', '2017-03-06 03:44:12', 'all-access'),
(19, 'CHEF RH', 'chef RH', 'Chef de ressource humaine', '2018-05-06 09:42:04', '2018-05-06 09:42:04', NULL),
(20, 'COORDINATRICE T.I', 'coordinatrice T.I', 'Coordinatrice T.I', '2018-05-06 09:45:08', '2018-05-06 09:45:08', NULL),
(21, 'COORDINATEUR T.I', 'coordinateur T.I', 'Coordinateur T.I', '2018-05-06 09:46:35', '2018-05-06 09:46:35', NULL),
(22, 'PARTICIPANT', 'participant', 'Participant', '2018-05-06 09:48:09', '2018-05-06 09:48:09', NULL),
(23, 'EMPLOYÉ', 'employé', 'Employé', '2018-05-06 09:48:55', '2018-05-06 09:48:55', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles1s`
--

CREATE TABLE `roles1s` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom_role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(8, 1, 32, '2018-04-19 18:12:49', '2018-04-19 18:12:49'),
(19, 1, 27, '2018-05-06 09:27:09', '2018-05-06 09:27:09'),
(20, 20, 28, '2018-05-06 10:40:47', '2018-05-06 10:40:47'),
(21, 22, 29, '2018-05-06 10:41:11', '2018-05-06 10:41:11'),
(22, 19, 30, '2018-05-06 10:41:39', '2018-05-06 10:41:39'),
(23, 21, 31, '2018-05-06 10:42:05', '2018-05-06 10:42:05'),
(25, 1, 5, '2019-04-25 21:01:32', '2019-04-25 21:01:32'),
(26, 22, 33, '2019-05-02 22:29:28', '2019-05-02 22:29:28'),
(27, 22, 34, '2019-05-03 19:55:05', '2019-05-03 19:55:05'),
(28, 23, 34, '2019-05-06 23:57:12', '2019-05-06 23:57:12');

-- --------------------------------------------------------

--
-- Structure de la table `semaine_plans`
--

CREATE TABLE `semaine_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_semaine` int(11) NOT NULL,
  `complete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participant_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `semaine_plans`
--

INSERT INTO `semaine_plans` (`id`, `no_semaine`, `complete`, `created_at`, `updated_at`, `participant_id`) VALUES
(1, 1, 1, '2019-04-25 23:55:32', '2019-04-25 23:55:32', 3),
(2, 2, 1, '2019-05-02 22:22:43', '2019-05-02 22:22:43', 3),
(3, 3, 1, '2019-05-02 22:30:58', '2019-05-02 22:30:58', 3),
(4, 4, 1, '2019-05-02 22:40:56', '2019-05-02 22:40:56', 3),
(5, 5, 1, '2019-05-02 22:44:34', '2019-05-02 22:44:34', 3),
(6, 6, 1, '2019-05-02 22:48:30', '2019-05-02 22:48:30', 3),
(7, 7, 1, '2019-05-03 17:39:03', '2019-05-03 17:39:03', 3),
(8, 1, 1, '2019-05-03 19:56:52', '2019-05-03 19:56:52', 4),
(9, 8, 1, '2019-05-07 23:26:49', '2019-05-07 23:26:49', 3);

-- --------------------------------------------------------

--
-- Structure de la table `suivis`
--

CREATE TABLE `suivis` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_suivi` date NOT NULL,
  `notes_suivi` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `participantSuivi_id` int(10) UNSIGNED NOT NULL,
  `employeResponsable_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prenoms` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `noms` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `prenoms`, `noms`, `telephone`) VALUES
(5, 'ADMINISTRATEUR ADMIN', 'admin@hotmail.com', '$2y$10$YeL/sPFQNRAy2blae.pYveCUuYCkOsvth/iKoFcMwWejci4/HWv9e', 'eo4WV6olmoTXX8vYjQLq9n6mGU1jqXmtSl49ab5uUYO8lpIHh1oMc4kyl0VB', '2018-04-19 18:09:26', '2019-05-07 22:25:29', 'ADMINISTRATEUR', 'ADMIN', '4189536811'),
(27, 'LARRY  NAVA', 'larry@hotmail.com', '$2y$10$LDCXTzZ2XZUKP.Auo4xgjOrsoKl3Y9eGhB1rSTWo3j5N557vanxxS', 'fkGtYdnFS1CmSp7itzByFq1pF44HMyr6gqj2b1gJeGmflK0T8xfJxXnYhaIW', '2018-05-06 05:26:34', '2019-05-02 22:53:56', 'LARRY ', 'NAVA', '4186934521'),
(28, 'ZOE BOUCHARD', 'Zoe@hotmail.com', '$2y$10$JtwSLNlSPY0jzPRLqCV95.K1HaX4xqxHV/62IS4TPz/D9maPJh1Yy', 'lWVC1wb1OkNsFO5a4dI7B9GVxCL1oF2mmxwA9exIcNxisYnmTmVYhlwgzL1X', '2018-05-06 06:37:19', '2019-05-07 23:26:20', 'ZOE', 'BOUCHARD', '4185632147'),
(29, 'JOSEPH TREMBLAY', 'joseph@hotmail.com', '$2y$10$R9YqSNfELvx.rwC4j9NFwuBjkOkcbrBqDqhFhD5TWmfwNh9QiKndG', 'Xm1PYzt5mkSOzctGM8cCwGoPrB1S4P1Jx56LEvEwbbWkcZtPFc08PGRVmjt5', '2018-05-06 06:38:19', '2019-05-02 22:54:33', 'JOSEPH', 'TREMBLAY', '4187534125'),
(30, 'ALICE PELLETIER', 'alice@hotmail.com', '$2y$10$qq18y7tXxDYkNUK6rw0l4eD4CKwTixBKwDrFE8t69fb/seUOJXXFO', '9cYkqYYA1qGRrtJx9wwAcEHefYizJgvLRfQDJtxooRVeRZ6RqqkAOsHk5VlW', '2018-05-06 06:39:14', '2019-05-07 21:00:11', 'ALICE', 'PELLETIER', '4183332222'),
(31, 'PIERRE GAGNON', 'pierre@hotmail.com', '$2y$10$EpvkH7K8zkcmlWVP7WXQJOan5sNamVAmYgCrGq8hZBqiG9QXnnSCi', 'YDhWoDj4GVBMiN0lahjvyDr4MIB7ZtnTgYKWLdb46kBbG8Llg5ZDkEQmg48R', '2018-05-06 06:40:03', '2019-05-02 22:55:05', 'PIERRE', 'GAGNON', '4181236589'),
(32, 'Denis Poisson', 'poisson_denis@yahoo.ca', '$2y$10$/o/ZbIVxT99tPLQEMZ8uIeHOqkg8Et9F55ecgXs9pKwKlCkG4JMbm', 'QxxalZLeLIaqihlMvK12Gk146w7jw5CuCjRwlzGvnVAJLXFqpVEeqy785O8t', '2019-04-08 22:43:31', '2019-05-06 19:55:21', '', '', ''),
(33, 'MILAN KUNDERA', 'milankundera@yahoo.ca', '$2y$10$Cy5ct4y3z25zVr.t3yn22.Dof.Y3BTP1TqMrT6kNVRvPDI8R0zxBm', 'c1cqZnt2fz3Y91PRGVM0L8Qc338tilRojKeQ2v6mJmXKcRsf4QL5WeWV1g3W', '2019-05-02 22:28:57', '2019-05-07 23:29:50', 'MILAN', 'KUNDERA', '14188500444'),
(34, 'PIERRE LABRIE', 'pierrelabrie@hotmail.com', '$2y$10$Ew6S1SUR1I9zZgTBl3NkCuKD5PA3GRVZaeSyKU7mxrY/7o.FpMLVC', 'Dcxzqbjlg4JXKcpM4sdCbY2fXeTv9NAU0jtwJtL0GVsCEnBbvQa8spm7hip4', '2019-05-03 19:54:22', '2019-05-07 18:37:02', 'PIERRE', 'LABRIE', '1231231234');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_employe_id_foreign` (`employe_id`);

--
-- Index pour la table `ateliers`
--
ALTER TABLE `ateliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ateliers_participantatelier_id_foreign` (`participantAtelier_id`);

--
-- Index pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `departements`
--
ALTER TABLE `departements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employes_departement_id_foreign` (`departement_id`),
  ADD KEY `employes_fonction_id_foreign` (`fonction_id`),
  ADD KEY `employes_coordonnees_id_foreign` (`coordonnees_id`);

--
-- Index pour la table `fonctions`
--
ALTER TABLE `fonctions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participants_coordonnees_id_foreign` (`coordonnees_id`);

--
-- Index pour la table `participant_emplois`
--
ALTER TABLE `participant_emplois`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participant_emplois_participantemploi_id_foreign` (`participantEmploi_id`);

--
-- Index pour la table `participant_integrations`
--
ALTER TABLE `participant_integrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participant_integrations_participant_id_foreign` (`participant_id`),
  ADD KEY `participant_integrations_departementintegration_id_foreign` (`departementIntegration_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Index pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Index pour la table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Index pour la table `plan_travails`
--
ALTER TABLE `plan_travails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_travails_participant_id_foreign` (`participant_id`);

--
-- Index pour la table `plan_travail_elements`
--
ALTER TABLE `plan_travail_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_travail_elements_departement_id_foreign` (`departement_id`),
  ADD KEY `plan_travail_elements_section_id_foreign` (`section_id`);

--
-- Index pour la table `plan_travail_element_participants`
--
ALTER TABLE `plan_travail_element_participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_travail_element_participants_element_id_foreign` (`element_id`),
  ADD KEY `plan_travail_element_participants_participant_id_foreign` (`participant_id`);

--
-- Index pour la table `plan_travail_element_semaine_plan`
--
ALTER TABLE `plan_travail_element_semaine_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_travail_element_semaine_plan_element_id_foreign` (`element_id`),
  ADD KEY `plan_travail_element_semaine_plan_semaine_plan_id_foreign` (`semaine_plan_id`);

--
-- Index pour la table `plan_travail_sections`
--
ALTER TABLE `plan_travail_sections`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Index pour la table `roles1s`
--
ALTER TABLE `roles1s`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Index pour la table `semaine_plans`
--
ALTER TABLE `semaine_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semaine_plans_participant_id_foreign` (`participant_id`);

--
-- Index pour la table `suivis`
--
ALTER TABLE `suivis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suivis_participantsuivi_id_foreign` (`participantSuivi_id`),
  ADD KEY `suivis_employeresponsable_id_foreign` (`employeResponsable_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ateliers`
--
ALTER TABLE `ateliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `departements`
--
ALTER TABLE `departements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `employes`
--
ALTER TABLE `employes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `fonctions`
--
ALTER TABLE `fonctions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `participant_emplois`
--
ALTER TABLE `participant_emplois`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `participant_integrations`
--
ALTER TABLE `participant_integrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT pour la table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `plan_travails`
--
ALTER TABLE `plan_travails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `plan_travail_elements`
--
ALTER TABLE `plan_travail_elements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `plan_travail_element_participants`
--
ALTER TABLE `plan_travail_element_participants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `plan_travail_element_semaine_plan`
--
ALTER TABLE `plan_travail_element_semaine_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `plan_travail_sections`
--
ALTER TABLE `plan_travail_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `roles1s`
--
ALTER TABLE `roles1s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `semaine_plans`
--
ALTER TABLE `semaine_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `suivis`
--
ALTER TABLE `suivis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employes` (`id`);

--
-- Contraintes pour la table `ateliers`
--
ALTER TABLE `ateliers`
  ADD CONSTRAINT `ateliers_participantatelier_id_foreign` FOREIGN KEY (`participantAtelier_id`) REFERENCES `participant_integrations` (`id`);

--
-- Contraintes pour la table `employes`
--
ALTER TABLE `employes`
  ADD CONSTRAINT `employes_coordonnees_id_foreign` FOREIGN KEY (`coordonnees_id`) REFERENCES `coordonnees` (`id`),
  ADD CONSTRAINT `employes_departement_id_foreign` FOREIGN KEY (`departement_id`) REFERENCES `departements` (`id`),
  ADD CONSTRAINT `employes_fonction_id_foreign` FOREIGN KEY (`fonction_id`) REFERENCES `fonctions` (`id`);

--
-- Contraintes pour la table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_coordonnees_id_foreign` FOREIGN KEY (`coordonnees_id`) REFERENCES `coordonnees` (`id`);

--
-- Contraintes pour la table `participant_emplois`
--
ALTER TABLE `participant_emplois`
  ADD CONSTRAINT `participant_emplois_participantemploi_id_foreign` FOREIGN KEY (`participantEmploi_id`) REFERENCES `plan_travail_elements` (`id`);

--
-- Contraintes pour la table `participant_integrations`
--
ALTER TABLE `participant_integrations`
  ADD CONSTRAINT `participant_integrations_departementintegration_id_foreign` FOREIGN KEY (`departementIntegration_id`) REFERENCES `departements` (`id`),
  ADD CONSTRAINT `participant_integrations_participant_id_foreign` FOREIGN KEY (`participant_id`) REFERENCES `participants` (`id`);

--
-- Contraintes pour la table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `plan_travails`
--
ALTER TABLE `plan_travails`
  ADD CONSTRAINT `plan_travails_participant_id_foreign` FOREIGN KEY (`participant_id`) REFERENCES `participants` (`id`);

--
-- Contraintes pour la table `plan_travail_elements`
--
ALTER TABLE `plan_travail_elements`
  ADD CONSTRAINT `plan_travail_elements_departement_id_foreign` FOREIGN KEY (`departement_id`) REFERENCES `departements` (`id`),
  ADD CONSTRAINT `plan_travail_elements_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `plan_travail_sections` (`id`);

--
-- Contraintes pour la table `plan_travail_element_participants`
--
ALTER TABLE `plan_travail_element_participants`
  ADD CONSTRAINT `plan_travail_element_participants_element_id_foreign` FOREIGN KEY (`element_id`) REFERENCES `plan_travail_elements` (`id`),
  ADD CONSTRAINT `plan_travail_element_participants_participant_id_foreign` FOREIGN KEY (`participant_id`) REFERENCES `participant_integrations` (`id`);

--
-- Contraintes pour la table `plan_travail_element_semaine_plan`
--
ALTER TABLE `plan_travail_element_semaine_plan`
  ADD CONSTRAINT `plan_travail_element_semaine_plan_element_id_foreign` FOREIGN KEY (`element_id`) REFERENCES `plan_travail_elements` (`id`),
  ADD CONSTRAINT `plan_travail_element_semaine_plan_semaine_plan_id_foreign` FOREIGN KEY (`semaine_plan_id`) REFERENCES `semaine_plans` (`id`);

--
-- Contraintes pour la table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `semaine_plans`
--
ALTER TABLE `semaine_plans`
  ADD CONSTRAINT `semaine_plans_participant_id_foreign` FOREIGN KEY (`participant_id`) REFERENCES `participants` (`id`);

--
-- Contraintes pour la table `suivis`
--
ALTER TABLE `suivis`
  ADD CONSTRAINT `suivis_employeresponsable_id_foreign` FOREIGN KEY (`employeResponsable_id`) REFERENCES `employes` (`id`),
  ADD CONSTRAINT `suivis_participantsuivi_id_foreign` FOREIGN KEY (`participantSuivi_id`) REFERENCES `participant_integrations` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
