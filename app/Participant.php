<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;


class Participant extends Authenticatable
{
  use Notifiable;

  protected $fillable = ['niveau_etudes','domaine_etudes',	'admins_personne' ];


  public function Coordonnees()
  {
    return $this->belongsTo('App\Coordonnees');
  }

  public function User()
  {
    return $this->hasOne('App\User');
  }
  
  public function PlanTravail()
  {
    return $this->hasOne('App\PlanTravail');
  }

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['password', 'remember_token'];
  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new AdminResetPasswordNotification($token));
  }

}
