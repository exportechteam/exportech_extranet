<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanTravail extends Model
{
  public function Participant()
  {
    return $this->belongsTo('App\Participant');
  }
}
