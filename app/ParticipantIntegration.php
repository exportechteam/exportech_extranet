<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class ParticipantIntegration extends Authenticatable
{
  use Notifiable;

  protected $fillable = [ 'id',
                          'photo',
                          'numero_participant',
                          'urgence_contact',
                          'urgence_coordonnees',
                          'periode',
                          'date_debut_stage',
                          'nombre_semaine_prolongation',
                          'nombre_semaine_absence',
                          'nombre_semaine_totale',
                          'date_fin_prevue_stage',
                          'date_fin_reelle_stage',
                          'reference',
                          'clientele',
                          'source_revenu',
                          'financement',
                          'no_dossier_emploiQC',
                          'nom_agent',
                          'adresse_agent',
                          'provenance',
                          'particularite',
                          'transport',
                          'commentaires',
                          'raison_depart',
                          'en_emploi',
                          'objectif_atteint',
                          'participant_id',
                          'departementIntegration_id' ];


  public function Participant()
  {
   return $this->belongsTo('App\Participant');
  }

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['password', 'remember_token'];
  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
      $this->notify(new AdminResetPasswordNotification($token));
  }
}
