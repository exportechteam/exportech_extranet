<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employe extends Model
{
  use Notifiable;

protected $fillable = [
    'departement_id', 'fonction_id', 'nom', 'email','password'
];

public function Coordonnees()
{
return $this->belongsTo('App\Coordonnees');
}

public function Departement()
{
return $this->belongsTo('App\Departement');
}

public function Fonction()
{
return $this->belongsTo('App\Fonction');
}

// public function User()
// {
//  return $this->belongsTo('Exportech_Extranet\User');
// }

public function User()
{
return $this->hasOne('App\User');
}

/**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

}
