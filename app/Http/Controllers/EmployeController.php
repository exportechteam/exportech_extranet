<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employe;
use App\Departement;
use App\Fonction;
use App\Coordonnees;
use Flash;

class EmployeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $employes = Employe::all();

      // création du tableau de résultats pour la recherche Vuejs
      $resultat = collect();
      foreach ($employes as $emp){
        // on assemble le nom complet de l'employé
        $bonNom = $emp->coordonnees->prenom." ".$emp->coordonnees->nom;
        // on pousse l'id, le nom, le departement et la fonction de l'employé dans le tableau
        $resultat -> push(['id'=> $emp->id, 'nom' => $bonNom, 'departement' => $emp->departement->nom_departement, 'fonction' => $emp->fonction->nom_fonction]);
        $resultat->all();
      }


      return view('employes.index')->with('resultat', $resultat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departements = Departement::all();
        $fonctions = Fonction::all();

        // on crée un tableau des départements pour le select dans la page create
        $select = [];
        foreach($departements as $departement){
            $select[$departement->id] = $departement->nom_departement;
        }

        $selectdeux = [];

        $selectdeux = [];
        foreach($fonctions as $fonction){
            $selectdeux[$fonction->id] = $fonction->nom_fonction;
        }

        return view('employes.create', compact(['select', 'selectdeux']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // validation de tous les champs dans create
      $this -> validate($request, [
          'prenom' => 'required|min:1|max:80',
          'nom' => 'required|min:1|max:80',
          'sexe' => 'required',
          'lieu_naissance' => 'min:1|max:80|required',
          'app' => 'min:1|nullable',
          'adresse' => 'min:2|max:255|required',
          'ville' => 'min:1|max:80|required',
          'province' => 'min:1|max:80|required',
          'code_postal' => array(
                'required',
                'regex:/[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]/'
            ),
          'telephone' => array(
                'required',
                'regex:/[0-9]{10}/'
            ),
            'date_naissance' => array(
                  'required',
                  'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'
              )

      ]);

      // var si l'employé n'a pas d'appartement
      $app = 0;

      if ($request->input('app') == null) {
        $app = 0;
      }
      else {
        $app = $request->input('app');
      }

      // on entre les coordonnées de l'employé qui seront stockées dans la table coordonnées
      $coordonnes = new Coordonnees;
      $coordonnes->adresse = $request->input('adresse');
      $coordonnes->appartement = $app;
      $coordonnes->ville = $request->input('ville');
      $coordonnes->province = $request->input('province');
      $coordonnes->code_postal = $request->input('code_postal');
      $coordonnes->telephone = $request->input('telephone');
      $coordonnes->prenom = $request->input('prenom');
      $coordonnes->nom = $request->input('nom');
      $coordonnes->sexe = $request->input('sexe');
      $coordonnes->lieu_naissance = $request->input('lieu_naissance');
      $coordonnes->date_naissance = $request->input('date_naissance');

      // si la sauvegarde de coordonnées fonctionne, on crée le nouvel employé
      if ($coordonnes->save()) {
        $employes = new Employe;
        $employes->departement_id = $request->input('departement');
        $employes->fonction_id = $request->input('fonction');
        $employes->isAdmin = 0;

        // sauvegarde de la clé étrangère coordonnees_id dans la table employé à l'aide de la fonction employes dans le modèle de coordonnees
        $coordonnes->employes()->save($employes);

        if ($employes->save()) {

          $users = new User;
        
          // création automatique du email de l'employé
          $users->email = strtolower($request->input('prenom')[0].'.'.$request->input('nom')."@exportechquebec.ca");
          // role 1 = employé
          // $users->role_id = 1;
          // un employé n'est pas un participant donc participant_id est null
         // $users->participant_id = null;
          $users->password = bcrypt('123456');

          // sauvegarde de la clé étrangère employe_id dans la table users à l'aide de la fonction user dans le modèle d'employe
          //$employes->user()->save($users);


          flash::success("Employé ajouté");

          return redirect('/');
        }



        return redirect('/');
      }

      return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $employes = Employe::find($id);
      $departements = Departement::all();
      $fonctions = Fonction::all();
      $coordonnees = Coordonnees::all();

      return view('employes.show', compact(['employes', 'departements', 'fonctions', 'coordonnees']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $employes = Employe::find($id);
      $departements = Departement::all();
      $fonctions = Fonction::all();
      $coordonnees = Coordonnees::all();


      $departements = Departement::all();
      $fonctions = Fonction::all();

      // on crée un tableau des départements pour le select dans la page create
      $select = [];
      foreach($departements as $departement){
          $select[$departement->id] = $departement->nom_departement;
      }

      $selectdeux = [];

      $selectdeux = [];
      foreach($fonctions as $fonction){
          $selectdeux[$fonction->id] = $fonction->nom_fonction;
      }

      return view('employes.edit', compact(['employes', 'select', 'selectdeux', 'departements', 'fonctions', 'coordonnees']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this -> validate($request, [
          'prenom' => 'required|min:1|max:80',
          'nom' => 'required|min:1|max:80',
          'sexe' => 'required',
          'lieu_naissance' => 'min:1|max:80|required',
          'app' => 'min:1|nullable',
          'adresse' => 'min:2|max:255|required',
          'ville' => 'min:1|max:80|required',
          'province' => 'min:1|max:80|required',
          'code_postal' => array(
                'required',
                'regex:/[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]/'
            ),
          'telephone' => array(
                'required',
                'regex:/[0-9]{10}/'
            ),
            'date_naissance' => array(
                  'required',
                  'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'
              )

      ]);

      $app = 0;
      $employes = Employe::find($id);
      $idcoo = $employes->coordonnees_id;


      if ($request->input('app') == null) {
        $app = 0;
      }
      else {
        $app = $request->input('app');
      }

      $coordonnes = Coordonnees::find($idcoo);
      $coordonnes->adresse = $request->input('adresse');
      $coordonnes->appartement = $app;
      $coordonnes->ville = $request->input('ville');
      $coordonnes->province = $request->input('province');
      $coordonnes->code_postal = $request->input('code_postal');
      $coordonnes->telephone = $request->input('telephone');
      $coordonnes->prenom = $request->input('prenom');
      $coordonnes->nom = $request->input('nom');
      $coordonnes->sexe = $request->input('sexe');
      $coordonnes->lieu_naissance = $request->input('lieu_naissance');
      $coordonnes->date_naissance = $request->input('date_naissance');

      $coordonnes->save();


      if ($coordonnes->save()) {
        $employes = Employe::find($id);

        $employes->departement_id = $request->input('departement');
        $employes->fonction_id = $request->input('fonction');

        $coordonnes->employes()->save($employes);

        flash::success("Employé modifié");

        return redirect('/');
      }

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
