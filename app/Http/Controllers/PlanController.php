<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanTravail;
use App\PlanTravailSection;
use App\PlanTravailElement;
use App\Participant;
use App\Coordonnees;
use App\SemainePlan;
use Flash;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $plans = PlanTravail::all();
      $resultat = collect();
      foreach ($plans as $plan){
              $resultat -> push(['id'=> $plan->id, 'nom' => $plan->participant->coordonnees->prenom.' '.$plan->participant->coordonnees->nom]);

        $resultat->all();
      }

      return view('plan_travails.index', compact('resultat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $participants = Participant::all();
      $sections = PlanTravailSection::all();
      $elements = PlanTravailElement::all();

      $select = [];
      foreach($participants as $participant){
          $select[$participant->id] = $participant->coordonnees->nom.' '.$participant->coordonnees->prenom;
      }

      return view('plan_travails.create', compact(['select', 'sections', 'elements']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan = new PlanTravail;
        $plan->participant_id = $request->input('participant');
        $plan->element_id = serialize($request->input('elements'));

        $plan->save();

        flash::success("Plan de travail créé");

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plans = PlanTravail::find($id);
        $sections = PlanTravailSection::all();
        $semaines = SemainePlan::where('participant_id', $plans->participant_id)->get();
        $totaux = [];
        $diff = [];


        foreach ($semaines as $sem) {
          foreach ($sem->PlanTravailElements as $element) {
            $diff[$element->id] = 0;
            if ($element->section_id == 2) {
              $totaux[$element->id] = 0;
            }
          }
        }

        foreach ($semaines as $sem) {
          foreach ($sem->PlanTravailElements as $element) {
          if ($element->section_id == 2) {
            $totaux[$element->id] += $element->pivot->pourcentage;
          }
          }
        }

        // dd($diff);

        // dd($totaux);

        // foreach ($semaines as $sem) {
        //   $tableau = unserialize($sem->element_pourcentage);
        //
        //   array_push($semaine_elements, $tableau);
        // }
        //
        // $totaux = $semaine_elements;
                //
        // $acc = array_shift($totaux);
        // foreach ($totaux as $val) {
        //     foreach ($val as $key => $val) {
        //         $acc[$key] += $val;
        //     }
        // }


        $elements = [];
        $diff2 = [];
        $plan_elements = unserialize($plans->element_id);

        foreach ($plan_elements as $plan) {
          $diff2[$plan] = 0;
          $valeur = PlanTravailElement::where('id', $plan)->get();


          array_push($elements, $valeur);
        }

        // calcul la différence entre les 2 tableaux pour voir quels éléments n'ont pas été travaillé
        $diff2 = array_diff_key($diff2, $diff);
        // dd($diff2);





        return view('plan_travails.show', compact(['sections', 'elements', 'plans', 'totaux', 'semaines', 'diff2']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $plans = PlanTravail::find($id);

      $participants = Participant::where('id', $plans->participant_id)->get();

      $sections = PlanTravailSection::all();

      $elements = PlanTravailElement::all();
      // $elements = [];
      //
      // $plan_elements = unserialize($plans->element_id);

      // foreach ($plan_elements as $plan_element) {
      //   $valeur = PlanTravailElement::where('id', $plan_element)->get();
      //
      //   // inclure pourcentage à la collection
      //   // $valeur[0]['pourcentage'] = 0;
      //
      //   array_push($elements, $valeur);
      // }

      $select = [];
      foreach($participants as $participant){
          $select[$participant->id] = $participant->coordonnees->nom.' '.$participant->coordonnees->prenom;
      }

      return view('plan_travails.edit', compact(['select', 'sections', 'elements', 'participants', 'plans']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $plan = PlanTravail::find($id);

      $plan->participant_id = $request->input('participant');
      $plan->element_id = serialize($request->input('elements'));

      $plan->save();

      flash::success("Plan de travail créé");

      return redirect('/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
