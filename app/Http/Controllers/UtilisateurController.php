<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Participant;
use App\Coordonnees;
use Illuminate\Support\Facades\Validator;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class UtilisateurController extends Controller
{


	/*
	 * Cette fonction charge un formulaire pour ajouter un nouvel utilisateur
	 */
	public function form_nouveau_utilisateur(){
	    //$role=Role::all();
	    //return view("formularios.form_nouveau_utilisateur")->with("role",$role);
	    return view("formulaires.form_nouveau_utilisateur");
	}



	/*
	 * Cette fonction charge un formulaire pour ajouter un nouvel rôle
	 */
	public function form_nouveau_role(){
	    $role=Role::all();
	    return view("formulaires.form_nouveau_role")->with("role",$role);
	}



	/*
	 * Cette fonction charge un formulaire pour ajouter une nouvelle permission
	 */
	public function form_nouveau_permis(){
		$role=Role::all();
		$permis=Permission::all();
	    return view("formulaires.form_nouveau_permis")->with("role",$role)->with("permis", $permis);
	}



	/*
	 * Cette fonction affiche une liste d'utilisateurs paginés de 100 dans 100
	 */
	public function lister_utilisateur(){

		$utilisateurs=User::paginate(100);
		return view("listees.liste_utilisateurs")->with("utilisateurs",$utilisateurs);
	}



	/*
	 * Cette fonction crée un nouvel utilisateur sur le système
	 */
	public function creer_utilisateur(Request $request){

		$regles=[  'password' => 'required|min:8',
		             'email' => 'required|email|unique:users', ];

		$messages=[  'password.min' => 'Le mot de passe doit être composé d\'au moins 8 caractères',
		             'email.unique' => 'Le courrier électronique existe déjà dans la base de données', ];

		$validator = Validator::make( $request->all(),$regles,$messages );
		if( $validator->fails() ){
		    return view("messages.message_erreur")->with("msj","...il y a erreurs...")
		                                        ->withErrors($validator->errors());
		}

		$utilisateur=new User;
		//$utilisateur->name=strtoupper( $request->input("prenom")." ".$request->input("nom") ) ;
		$utilisateur->email=$request->input("email");
		$utilisateur->password= bcrypt( $request->input("password") );


	    if($utilisateur->save())
	    {
	      return view("messages.msj_utilisateur_cree")->with("msj","l'utilisateur a été correctement ajouté") ;
	    }
	    else
	    {
	        return view("messages.message_erreur")->with("msj","...Il y a eu une erreur après avoir ajouté ;...") ;
	    }
	}


	/*
	 * Cette fonction crée un nouvel rôle sur le système
	 */
	public function creer_role(Request $request){

	   $rol=new Role;
	   $rol->name=$request->input("rol_nom") ;
	   $rol->slug=$request->input("rol_slug") ;
	   $rol->description=$request->input("rol_description") ;
	    if($rol->save())
	    {
	        return view("messages.msj_role_cree")->with("msj","Rôle ajouté correctement") ;
	    }
	    else
	    {
	        return view("messages.message_erreur")->with("msj","...il y a eu une erreur après avoir ajouté le rôle ;...") ;
	    }
	}


	/*
	 * Cette fonction crée un nouvel permis sur le système
	 */
	public function creer_permis(Request $request){
	   $permis=new Permission;
	   $permis->name=$request->input("permis_nom") ;
	   $permis->slug=$request->input("permis_slug") ;
	   $permis->description=$request->input("permis_description") ;
	    if($permis->save())
	    {
	        return view("messages.msj_permis_cree")->with("msj","La permission a été correctement créée") ;
	    }
	    else
	    {
	        return view("messages.message_erreur")->with("msj","...il y a eu une erreur après avoir ajouté la permission;...") ;
	    }
	}

	/*
	 * Cette fonction assigne une nouvelle permission  à un rôle
	 */
	public function assigner_permis(Request $request){

	     $roleid=$request->input("rol_sel");
	     $idper=$request->input("permis_rol");
	     $rol=Role::find($roleid);
	     $rol->assignPermission($idper);

	    if($rol->save())
	    {
	        return view("messages.msj_permis_cree")->with("msj","La permission a été correctement assignée") ;
	    }
	    else
	    {
	        return view("messages.message_erreur")->with("msj","...il y a eu une erreur après avoir assignée la permission  ;...") ;
	    }
	}


	/*
	 * Cette fonction édite les données d'un utilisateurs
	 */
	public function form_editer_utilisateur($id){

		$utilisateur=User::find($id);
		$role=Role::all();
			
	    return view("formulaires.form_editer_utilisateur")->with("utilisateur",$utilisateur)
		                                              ->with("role",$role);
	}


	/*
	 * Cette fonction actualise les données d'un utilisateurs
	 */
	public function editer_utilisateur(Request $request){

	    $idutilisateur=$request->input("id_utilisateur");
		$utilisateur=User::find($idutilisateur);
		//$utilisateur->name=strtoupper( $request->input("prenom")." ".$request->input("nom") ) ;
		
		
		//$utilisateur->participant->coordonnees->prenom=$request->input("prenom");
		//$utilisateur->participant->coordonnees->nom=$request->input("nom");
	    
	     if($request->has("rol")){
		    $rol=$request->input("rol");
		    $utilisateur->revokeAllRoles();
		    $utilisateur->assignRole($rol);
	     }

	     if( $utilisateur->save()){
			return view("messages.msj_utilisateur_actualise")->with("msj","L'utilisateur a été correctement actualisé")
		                                                   ->with("idutilisateur",$idutilisateur) ;
	     }
	     else
	     {
			return view("messages.message_erreur")->with("msj","..il y a eu une erreur dans l'actualisation; essayez encore une fois, s'il vous plait..");
	     }
	}



	/*
	 * Cette fonction cherche un utilisateurs dans la base de données
	 */
	public function chercher_utilisateur(Request $request){
		$donnee=$request->input("donnee_cherchee");
		$utilisateurs=User::where("name","like","%".$donnee."%")->orwhere("apellidos","like","%".$donnee."%")                                              ->paginate(100);
		return view('listees.liste_utilisateurs')->with("utilisateurs",$utilisateurs);
	}



	/*
	 * Cette fonction élimine un utilisateur dans la base de données
	 */
	public function supprimer_utilisateur(Request $request){

        $idutilisateur=$request->input("id_utilisateur");
        $utilisateur=User::find($idutilisateur);

        if($utilisateur->delete()){
             return view("messages.msj_utilisateur_elimine")->with("msj","L'utilisateur a été correctement éliminé") ;
        }
        else
        {
            return view("messages.message_erreur")->with("msj","..il y a eu une erreur après avoir éliminé; essayez encore une fois, s'il vous plait..");
        }
	}


	/*
	 * Cette fonction édite le courriel et mot de passe d'un utilisateur
	 */
	public function editer_acces(Request $request){
         $idutilisateur=$request->input("id_utilisateur");
         $utilisateur=User::find($idutilisateur);
         $utilisateur->email=$request->input("email");
         $utilisateur->password= bcrypt( $request->input("password") );
          if( $utilisateur->save()){
        return view("messages.msj_utilisateur_actualise")->with("msj","l'utilisateur a été correctement actualisé")->with("idutilisateur",$idutilisateur) ;
         }
          else
          {
        return view("messages.message_erreur")->with("msj","...il y a eu une erreur dans l'actualisation; essayez encore une fois, s'il vous plait..") ;
          }
	}


	/*
	 * Cette fonction assigne un rôle à un utilisateur
	 */
	public function assigner_role($idusu,$idrol){

        $utilisateur=User::find($idusu);
        $utilisateur->assignRole($idrol);

        $utilisateur=User::find($idusu);
        $rolesassignes=$utilisateur->getRoles();

        return json_encode ($rolesassignes);
	}


	/*
	 * Cette fonction élimine un role à un utilisateur
	 */
	public function eliminer_role($idusu,$idrol){

	    $utilisateur=User::find($idusu);
	    $utilisateur->revokeRole($idrol);
	    $rolesassignes=$utilisateur->getRoles();
	    return json_encode ($rolesassignes);
	}

	/*
	 * Cette fonction affiche un message de confirmation pour éliminer un utilisateur
	 */
	public function form_supprimer_utilisateur($id){
	  $utilisateur=User::find($id);
	  return view("confirmations.form_elimine_utilisateur")->with("utilisateur",$utilisateur);
	}


	/*
	 * Cette fonction élimine une permission à un rôle
	 */
	public function supprimer_permis($idrole,$idper){

	    $role = Role::find($idrole);
	    $role->revokePermission($idper);
	    $role->save();

	    return "ok";
	}


	/*
	 * Cette fonction élimine un rôle
	 */
	public function supprimer_role($idrole){

	    $role = Role::find($idrole);
	    $role->delete();
	    return "ok";
	}
}
