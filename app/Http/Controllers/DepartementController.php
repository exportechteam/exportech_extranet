<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departement;
use Flash;

class DepartementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $departements = Departement::orderBy('created_at', 'desc')->get();
      return view('departements.index', compact('departements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this -> validate($request, [
          'nom_departement' => 'required|min:1|max:255',
      ]);


      $departements = new Departement;
      $departements->nom_departement = $request->input('nom_departement');
      $departements->nombre_semaine_integration = $request->input('nombre_semaine_integration');
      $departements->max_semaine_prolongation = $request->input('max_semaine_prolongation');
      $departements->save();

      flash::success("Département ajouté");

      return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $departements = Departement::find($id);

      return view('departements.show', compact('departements'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $departements = Departement::find($id);

      return view('departements.edit', compact('departements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this -> validate($request, [
          'nom_departement' => 'required|min:1|max:255',
      ]);


      $departements = Departement::find($id);
      $departements->nom_departement = $request->input('nom_departement');
      $departements->nombre_semaine_integration = $request->input('nombre_semaine_integration');
      $departements->max_semaine_prolongation = $request->input('max_semaine_prolongation');
      $departements->save();

      flash::success("Département modifié");

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
