<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanTravailElement;
use App\PlanTravailSection;
use App\Departement;
use Flash;

class PlanTravailElementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $elements = PlanTravailElement::all();

      $resultat = collect();
      foreach ($elements as $element){

        $resultat -> push(['id'=> $element->id, 'nom' => $element->nom_elements, 'departement' => $element->departement->nom_departement, 'section' => $element->section->nom_section]);

        $resultat->all();
      }

      return view('plan_travail_elements.index', compact('resultat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $departements = Departement::all();
      $sections = PlanTravailSection::all();
      $select = [];
      $selectDeux = [];

      foreach($departements as $dep){
          $select[$dep->id] = $dep->nom_departement;
      }

      foreach($sections as $sec){
          $selectDeux[$sec->id] = $sec->nom_section;
      }


        return view('plan_travail_elements.create', compact(['select', 'selectDeux']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan = new PlanTravailElement;
        $plan->nom_elements = $request->input('nom_elements');
        $plan->description_element = $request->input('description_element');
        $plan->actif_element = 1;
        $plan->departement_id = $request->input('departement');
        $plan->section_id = $request->input('section');
        $plan->save();

        flash::success("Élément créé");

        return redirect('/');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $elements = PlanTravailElement::find($id);
      $departement = Departement::where('id', $elements->departement_id)->get();

      $section = PlanTravailSection::where('id', $elements->section_id)->get();

      return view('plan_travail_elements.show', compact(['elements', 'section', 'departement']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $elements = PlanTravailElement::find($id);
      $departements = Departement::all();
      $sections = PlanTravailSection::all();
      $select = [];
      $selectDeux = [];

      foreach($departements as $dep){
          $select[$dep->id] = $dep->nom_departement;
      }

      foreach($sections as $sec){
          $selectDeux[$sec->id] = $sec->nom_section;
      }


        return view('plan_travail_elements.edit', compact(['select', 'selectDeux', 'elements']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $plan = PlanTravailElement::find($id);
      $plan->nom_elements = $request->input('nom_elements');
      $plan->description_element = $request->input('description_element');
      $plan->actif_element = $request->input('actif');
      $plan->departement_id = $request->input('departement');
      $plan->section_id = $request->input('section');
      $plan->save();

      flash::success("Élément modifié");

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $elements = Article::find($id);
      $elements->actif_element = 0;
      return redirect('/')->with('success', 'Element inactif');
    }
}
