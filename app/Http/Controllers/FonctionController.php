<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fonction;
use Flash;


class FonctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $fonctions = Fonction::orderBy('created_at', 'desc')->get();
      return view('fonctions.index', compact('fonctions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fonctions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this -> validate($request, [
          'nom_fonction' => 'required|min:1|max:255',
      ]);


      $fonctions = new Fonction;
      $fonctions->nom_fonction = $request->input('nom_fonction');
      $fonctions->save();

      flash::success("Fonction ajoutée");

      return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $fonctions = Fonction::find($id);

      return view('fonctions.edit', compact('fonctions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this -> validate($request, [
          'nom_fonction' => 'required|min:1|max:255',
      ]);


      $fonctions = Fonction::find($id);
      $fonctions->nom_fonction = $request->input('nom_fonction');
      $fonctions->save();

      flash::success("Fonction modifiée");

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
