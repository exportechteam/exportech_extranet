<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Participant;
use App\Coordonnees;
use Auth;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Flash;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $participants = Participant::all();

      $resultat = collect();
      foreach ($participants as $participant){
        $bonNom = $participant->coordonnees->prenom." ".$participant->coordonnees->nom;
        $resultat -> push(['id'=> $participant->id,
                           'nom' => $bonNom,
                           'niveau_etudes' => $participant->niveau_etudes,
                           'domaine_etudes' => $participant->domaine_etudes,
                           'created_at' => $participant->created_at,
                           'admis_personne' => $participant->admis_personne,
                          ]);
        $resultat->all();
      }

      //dd($participants);
      //$coordonnees = Coordonnees::all();
      return view('participants.index')->with('resultat', $resultat);   //, compact(['participants','coordonnees']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('participants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $date=date_create_from_format("Y-m-j");
      $regles=[  'email' => 'required|email|unique:users', ];

      $messages=[  'email.unique' => 'Le courrier électronique existe déjà dans la base de données', ];

      $validator = Validator::make( $request->all(),$regles,$messages );
      if( $validator->fails() ){
          return view("messages.message_erreur")->with("msj","...il y a erreurs...")
                                              ->withErrors($validator->errors());
      }

      $this -> validate($request, [
          //'prenom' => 'required|min:1|max:80',
          'prenom' => array(
            'required',
            'regex:/^\D+$/'
        ),
          //'nom' => 'required|min:1|max:80',
          'nom' => array(
            'required',
            'regex:/^\D+$/'
        ),
          'sexe' => 'required',
          'lieu_naissance' => array(
            'required',
            'regex:/^\D+$/'
        ),
          'app' => 'min:1|nullable',
          'adresse' => 'min:2|max:255|required',
          'ville' => array(
            'required',
            'regex:/^\D+$/'
        ),
          'province' => array(
            'required',
            'regex:/^\D+$/'
        ),
          'code_postal' => array(
                'required',
               //  'regex:/[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]/'
                 'regex:/([ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ])\ ?([0-9][ABCEGHJKLMNPRSTVWXYZ][0-9])/'
            ),
          'telephone' => array(
                'required',
               // 'regex:/[0-9]{10}/'
                'regex:/(?:^(?:(?:\+?1[\s])|(?:\+?1(?=(?:\()|(?:\d{10})))|(?:\+?1[\-](?=\d)))?(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$)|(?:^[2-9]\d{2}[- ]?\d{4}$)/'
            ),
            'date_naissance' => array(
                  'required',
                  'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'
              )

      ]);

      $app = 0;

      if ($request->input('app') == null) {
        $app = 0;
      }
      else {
        $app = $request->input('app');
      }

      $coordonnes = new Coordonnees;
      $coordonnes->adresse = $request->input('adresse');
      $coordonnes->appartement = $app;
      $coordonnes->ville = $request->input('ville');
      $coordonnes->province = $request->input('province');
      $coordonnes->code_postal = $request->input('code_postal');
      $coordonnes->telephone = $request->input('telephone');
      $coordonnes->prenom = $request->input('prenom');
      $coordonnes->nom = $request->input('nom');
      $coordonnes->sexe = $request->input('sexe');
      $coordonnes->lieu_naissance = $request->input('lieu_naissance');
      $coordonnes->date_naissance = $request->input('date_naissance');
      $coordonnes->email = $request->input('email');

      //$coordonnes->save();

      if ($coordonnes->save()) {
        $participants = new Participant;
        $participants->niveau_etudes = $request->input('niveau_etudes');
        $participants->domaine_etudes = $request->input('domaine_etudes');
        $participants->admis_personne = 0;

        $coordonnes->participants()->save($participants);

        flash::success("Participant ajouté");

        return redirect('/participants');
      }

      return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // Validation pour empêcher de changer le id du participant dans l'URL

        if ( Auth::user()->participant_id == $id || Auth::user()->can('participant.index')) {
          $participants = Participant::find($id);
          $coordonnees = Coordonnees::all();
          return view('participants.show', compact(['participants', 'coordonnees']));
        }
        else  {
          $id = Auth::user()->participant_id;
          $participants = Participant::find($id);
          $coordonnees = Coordonnees::all();
          return redirect('/participants/'.$id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $participants = Participant::find($id);
      $coordonnees = Coordonnees::find($participants->coordonnees_id);

      return view('participants.edit', compact(['participants', 'coordonnees']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $regles=[  'email' => 'required|email|unique:users', ];
      $messages=[  'email.unique' => 'Le courrier électronique existe déjà dans la base de données USERS', ];

      $this -> validate($request, [
          'prenom' => 'required|min:1|max:80',
          'nom' => 'required|min:1|max:80',
          'sexe' => 'required',
          'email' => 'required|email',
          'lieu_naissance' => 'min:1|max:80|required',
          'app' => 'min:1|nullable',
          'adresse' => 'min:2|max:255|required',
          'ville' => 'min:1|max:80|required',
          'province' => 'min:1|max:80|required',
          'code_postal' => array(
                'required',
                'regex:/[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9][ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][0-9]/'
            ),
          'telephone' => array(
                'required',
                'regex:/[0-9]{10}/'
            ),
            'date_naissance' => array(
                  'required',
                  'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'
              )

      ]);

      $app = 0;
      $participants = Participant::find($id);
      $idcoo = $participants->coordonnees_id;

      $coordonnes = Coordonnees::find($idcoo);

      if ($request->input('email') != $coordonnes->email){
        $validator = Validator::make( $request->all(),$regles,$messages );
        if( $validator->fails() ){
            return view("messages.message_erreur")->with("msj","...il y a erreurs...")
                                                ->withErrors($validator->errors());
        }
      }

      if ($request->input('app') == null) {
        $app = 0;
      }
      else {
        $app = $request->input('app');
      }

      $coordonnes->adresse = $request->input('adresse');
      $coordonnes->appartement = $app;
      $coordonnes->ville = $request->input('ville');
      $coordonnes->province = $request->input('province');
      $coordonnes->code_postal = $request->input('code_postal');
      $coordonnes->telephone = $request->input('telephone');
      $coordonnes->prenom = $request->input('prenom');
      $coordonnes->nom = $request->input('nom');
      $coordonnes->sexe = $request->input('sexe');
      $coordonnes->lieu_naissance = $request->input('lieu_naissance');
      $coordonnes->date_naissance = $request->input('date_naissance');
      $coordonnes->email = $request->input('email');
         
      if ($coordonnes->save()) {
        $participants = Participant::find($id);
        $participants->niveau_etudes = $request->input('niveau_etudes');
        $participants->domaine_etudes = $request->input('domaine_etudes');
        $coordonnes->participants()->save($participants);
        
        // On vérifie si le participant a été intégré (donc son User a été créé, il faut mettre à jour le email dans la table users)  
        if ($participants->admis_personne == 1)  {

          $utilisateurs = User::where('participant_id',$id)->first();
          $utilisateurs->email = $request->input('email');
          $utilisateurs->save();
        }

        flash::success("Participant modifié");
       
        if (Auth::user()->can('participant.index')) {
          return redirect('/participants');
        }  
        else  {
          return redirect('/');  
        }    
      }

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
