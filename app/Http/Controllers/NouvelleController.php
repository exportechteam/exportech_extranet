<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Flash;

class NouvelleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nouvelles = Article::orderBy('created_at', 'desc')->get();
        return view('nouvelles.index', compact('nouvelles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nouvelles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this -> validate($request, [
          'titre' => 'required|min:3|max:255',
          'contenu' => 'required',
          'image' => 'required'
      ]);

      if ($request->hasFile('image')) {

        // Avoir le nom du fichier avec l'extention
        $filenameWithExt = $request->file('image')->getClientOriginalName();
        // Avoir seulement le nom du fichier
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Avoir seulement le nom de l'extension
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $filename.'.'.$extension;
        // Télécharger l'image
        $path = $request->file('image')->move(public_path().'/img', $fileNameToStore);

      }
      else {
        $fileNameToStore = 'noimage.jpg';
      }


      $nouvelles = new Article;
      $nouvelles->employe_id = 1;
      $nouvelles->titre = $request->input('titre');
      $nouvelles->contenu = $request->input('contenu');
      $nouvelles->image = $fileNameToStore;
      $nouvelles->save();

      flash::success("Nouvelle ajoutée");

      return redirect('/');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $nouvelles = Article::find($id);
      // $users = User::all();

      return view('nouvelles.show', compact('nouvelles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nouvelles = Article::find($id);

        return view('nouvelles.edit', compact('nouvelles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this -> validate($request, [
          'titre' => 'required|min:3|max:255',
          'contenu' => 'required'
      ]);



      if ($request->hasFile('image')) {

        // Avoir le nom du fichier avec l'extention
        $filenameWithExt = $request->file('image')->getClientOriginalName();
        // Avoir seulement le nom du fichier
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Avoir seulement le nom de l'extension
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $filename.'.'.$extension;
        // Télécharger l'image
        $path = $request->file('image')->move(public_path().'/img', $fileNameToStore);

      }
      else {
        $fileNameToStore = 'noimage.jpg';
      }

      $nouvelles = Article::find($id);
      $nouvelles->titre = $request->input('titre');
      $nouvelles->contenu = $request->input('contenu');
      if($request->hasFile('image')){
        $nouvelles->image = $fileNameToStore;
      }

      $nouvelles->save();

      flash::success("Nouvelle ajoutée");

      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nouvelle = Article::find($id);
        $nouvelle->delete();
        return redirect('/')->with('success', 'Nouvelle supprimée');
    }
}
