<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Role;
use App\Article;

class PageController extends Controller
{

  public function home()
    {
		    // $nouvelles = Article::all();
        $nouvelles = Article::orderBy('created_at', 'desc')->get();

        //return view('welcome', compact('nouvelles'));
        return view('home', compact('nouvelles'));
    }
}
