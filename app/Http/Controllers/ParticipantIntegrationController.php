<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Caffeinated\Shinobi\Models\Role;
use App\ParticipantIntegration;
use App\Participant;
use App\Coordonnees;
use App\Departement;
use Flash;

class ParticipantIntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $participants = Participant::all();
      $coordonnees = Coordonnees::all();
      return view('participants.index', compact(['participants','coordonnes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
      $participant = Participant::find($id);
      $coordonnees = Coordonnees::find($participant->coordonnees_id);
      $departements = Departement::pluck('nom_departement','id');
      return view('participant_integrations.create', compact(['participant','coordonnees','departements']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $date=date_create_from_format("Y-m-j");

       $this -> validate($request, [ 'photo' => 'file|image|mimes:jpeg,png,gif,webp|max:2048',
                                     'numero_participant' => 'required|numeric',
                                     'urgence_contact' => array(
                                      'required',
                                      'regex:/^\D+$/'
                                  ),
                                    'urgence_coordonnees' => array(
                                      'required',
                                     // 'regex:/[0-9]{10}/'
                                      'regex:/(?:^(?:(?:\+?1[\s])|(?:\+?1(?=(?:\()|(?:\d{10})))|(?:\+?1[\-](?=\d)))?(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$)|(?:^[2-9]\d{2}[- ]?\d{4}$)/'
                                  ),
                                    'periode' => 'min:1|max:255',
                                    'date_debut_stage' => array('required',
                                                                'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'nombre_semaine_prolongation' => 'numeric',
                                    'nombre_semaine_absence' => 'numeric',
                                    'nombre_semaine_totale' => 'numeric',
                                    'date_fin_prevue_stage' => array('required',
                                                                     'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'date_fin_reelle_stage' => array('required',
                                                                     'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'reference' => 'min:1|max:400',
                                    'clientele' => 'min:1|max:400',
                                    'source_revenu' => 'min:1|max:400',
                                    'financement' => 'min:1|max:400',
                                    'no_dossier_emploiQC' => 'numeric',
                                    'nom_agent' => array(
                                      'required',
                                      'regex:/^\D+$/'
                                  ),
                                    'adresse_agent' => 'min:1|max:400',
                                    'provenance' => 'min:1|max:400',
                                    'particularite' => 'min:1|max:400',
                                    'transport' => 'min:1|max:400',
                                    'commentaires' => 'min:1|max:400',
                                    'raison_depart' => 'min:1|max:400',
                                    'en_emploi' => 'min:0|max:1',
                                    'objectif_atteint' => 'min:0|max:1'
                                   ]);


      $participantInt = new ParticipantIntegration;

      if ($request->hasFile('photo')) {

        // Avoir le nom du fichier avec l'extention
        $filenameWithExt = $request->file('photo')->getClientOriginalName();
        // Avoir seulement le nom du fichier
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Avoir seulement le nom de l'extension
        $extension = $request->file('photo')->getClientOriginalExtension();
        $fileNameToStore = $filename.'.'.$extension;
        // Télécharger photo
        $path = $request->file('photo')->move(public_path().'/img', $fileNameToStore);

      }
      else {
        $fileNameToStore = 'noimage.jpg';
      }
      $participantInt->photo = $fileNameToStore;
      $participantInt->numero_participant = $request->input('numero_participant');
      $participantInt->urgence_contact = $request->input('urgence_contact');
      $participantInt->urgence_coordonnees = $request->input('urgence_coordonnees');
      $participantInt->periode = $request->input('periode');
      $participantInt->date_debut_stage = $request->input('date_debut_stage');
      $participantInt->nombre_semaine_prolongation = $request->input('nombre_semaine_prolongation');
      $participantInt->nombre_semaine_absence = $request->input('nombre_semaine_absence');
      $participantInt->nombre_semaine_totale = $request->input('nombre_semaine_totale');
      $participantInt->date_fin_prevue_stage = $request->input('date_fin_prevue_stage');
      $participantInt->date_fin_reelle_stage = $request->input('date_fin_reelle_stage');
      $participantInt->reference = $request->input('reference');
      $participantInt->clientele = $request->input('clientele');
      $participantInt->source_revenu = $request->input('source_revenu');
      $participantInt->financement = $request->input('financement');
      $participantInt->no_dossier_emploiQC = $request->input('no_dossier_emploiQC');
      $participantInt->nom_agent = $request->input('nom_agent');
      $participantInt->adresse_agent = $request->input('adresse_agent');
      $participantInt->provenance = $request->input('provenance');
      $participantInt->particularite = $request->input('particularite');
      $participantInt->transport = $request->input('transport');
      $participantInt->commentaires = $request->input('commentaires');
      $participantInt->raison_depart = $request->input('raison_depart');
      $participantInt->en_emploi = $request->input('en_emploi');
      $participantInt->objectif_atteint = $request->input('objectif_atteint');

      $participantInt->participant_id = $request->input('participant_id');
      $participantInt->departementIntegration_id = 1;

      if($participantInt->save()) {
        $participant = Participant::find($request->input('participant_id'));
        $participant->admis_personne = 1;
        $participant->save();

        if ($participant->save()) {
          $coordonnees = Coordonnees::find($participant->coordonnees_id);
          $users = new User;
        
         //  $users->email = strtolower('participant'.$request->input('numero_participant')."@exportechquebec.ca");
                      
     
          $users->password = bcrypt('123456');
          $users->name = strtoupper($coordonnees->prenom)." ".strtoupper($coordonnees->nom);
          $users->employe_id = null;
          $users->participant_id =  $participant->id;
          $users->email = $coordonnees->email;
          
          //$users->save();
          $participant->user()->save($users);
          $role = Role::where('slug', 'participant')->first();
          $users->assignRole($role->id);

          flash::success("Participant intégré avec succès");

          return redirect('/participants');
        }
        return redirect('/');
      }


      flash::success("Participant Intégré");
      return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $participants = Participant::find($id);
      $coordonnees = Coordonnees::find($participants->coordonnees_id);
      $integration = ParticipantIntegration::where('participant_id', $participants->id)->first();
      return view('participant_integrations.show', compact(['participants', 'coordonnees', 'integration']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $participants = Participant::find($id);
      $coordonnees = Coordonnees::find($participants->coordonnees_id);
      $integration = ParticipantIntegration::where('participant_id', $participants->id)->first();
      return view('participant_integrations.edit', compact(['participants', 'coordonnees', 'integration']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this -> validate($request, [ 'photo' => 'file|image|mimes:jpeg,png,gif,webp|max:2048',
                                    'numero_participant' => 'numeric',
                                    'urgence_contact' => array(
                                      'required',
                                      'regex:/^\D+$/'
                                  ),
                                    'urgence_coordonnees' => array(
                                      'required',
                                     // 'regex:/[0-9]{10}/'
                                      'regex:/(?:^(?:(?:\+?1[\s])|(?:\+?1(?=(?:\()|(?:\d{10})))|(?:\+?1[\-](?=\d)))?(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$)|(?:^[2-9]\d{2}[- ]?\d{4}$)/'
                                  ),
                                    'periode' => 'min:1|max:255',
                                    'date_debut_stage' => array('required',
                                                                'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'nombre_semaine_prolongation' => 'numeric',
                                    'nombre_semaine_absence' => 'numeric',
                                    'nombre_semaine_totale' => 'numeric',
                                    'date_fin_prevue_stage' => array('required',
                                                                     'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'date_fin_reelle_stage' => array('required',
                                                                     'regex:/[0-9]{4}[\-][0-9]{2}[\-][0-9]{2}/'),
                                    'reference' => 'min:1|max:400',
                                    'clientele' => 'min:1|max:400',
                                    'source_revenu' => 'min:1|max:400',
                                    'financement' => 'min:1|max:400',
                                    'no_dossier_emploiQC' => 'numeric',
                                    'nom_agent' => array(
                                      'required',
                                      'regex:/^\D+$/'
                                  ),
                                    'adresse_agent' => 'min:1|max:400',
                                    'provenance' => 'min:1|max:400',
                                    'particularite' => 'min:1|max:400',
                                    'transport' => 'min:1|max:400',
                                    'commentaires' => 'min:1|max:400',
                                    'raison_depart' => 'min:1|max:400',
                                    'en_emploi' => 'min:0|max:1',
                                    'objectif_atteint' => 'min:0|max:1'
                                  ]);

                                  $participantInt = ParticipantIntegration::where('participant_id', $id)->first();

                                  if ($request->hasFile('photo')) {

                                    // Avoir le nom du fichier avec l'extention
                                    $filenameWithExt = $request->file('photo')->getClientOriginalName();
                                    // Avoir seulement le nom du fichier
                                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                                    // Avoir seulement le nom de l'extension
                                    $extension = $request->file('photo')->getClientOriginalExtension();
                                    $fileNameToStore = $filename.'.'.$extension;
                                    // Télécharger photo
                                    $path = $request->file('photo')->move(public_path().'/img', $fileNameToStore);

                                  }
                                  else {
                                    $fileNameToStore = 'noimage.jpg';
                                  }

                                  if($request->hasFile('photo')){
                                    $participantInt->photo = $fileNameToStore;
                                  }

                                  $participantInt->numero_participant = $request->input('numero_participant');
                                  $participantInt->urgence_contact = $request->input('urgence_contact');
                                  $participantInt->urgence_coordonnees = $request->input('urgence_coordonnees');
                                  $participantInt->periode = $request->input('periode');
                                  $participantInt->date_debut_stage = $request->input('date_debut_stage');
                                  $participantInt->nombre_semaine_prolongation = $request->input('nombre_semaine_prolongation');
                                  $participantInt->nombre_semaine_absence = $request->input('nombre_semaine_absence');
                                  $participantInt->nombre_semaine_totale = $request->input('nombre_semaine_totale');
                                  $participantInt->date_fin_prevue_stage = $request->input('date_fin_prevue_stage');
                                  $participantInt->date_fin_reelle_stage = $request->input('date_fin_reelle_stage');
                                  $participantInt->reference = $request->input('reference');
                                  $participantInt->clientele = $request->input('clientele');
                                  $participantInt->source_revenu = $request->input('source_revenu');
                                  $participantInt->financement = $request->input('financement');
                                  $participantInt->no_dossier_emploiQC = $request->input('no_dossier_emploiQC');
                                  $participantInt->nom_agent = $request->input('nom_agent');
                                  $participantInt->adresse_agent = $request->input('adresse_agent');
                                  $participantInt->provenance = $request->input('provenance');
                                  $participantInt->particularite = $request->input('particularite');
                                  $participantInt->transport = $request->input('transport');
                                  $participantInt->commentaires = $request->input('commentaires');
                                  $participantInt->raison_depart = $request->input('raison_depart');
                                  $participantInt->en_emploi = $request->input('en_emploi');
                                  $participantInt->objectif_atteint = $request->input('objectif_atteint');

                                  if ($participantInt->save()) {
                                    // $participants = Participant::find($id);
                                    // // $Participants->save();
                                    //
                                    // $coordonnes->participants()->save($participants);

                                    flash::success("Intégration du participant modifiée");

                                    return redirect('/participants');
                                  }

                                  return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
