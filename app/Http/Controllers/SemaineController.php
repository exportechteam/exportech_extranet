<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanTravail;
use App\PlanTravailSection;
use App\PlanTravailElement;
use App\Participant;
use App\SemainePlan;
use Illuminate\Support\Facades\Input;
use Flash;

class SemaineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

      $id_du_participant = $id;
      $nbrSemainePlans = SemainePlan::where('participant_id', $id)->get();
      $no_semaine = count($nbrSemainePlans);

      if ($no_semaine == 0) {
        $no_semaine = 1;
      }
      else{
        $no_semaine = $no_semaine+1;
      }




      $plans = PlanTravail::where('participant_id', $id)->get();
      $sections = PlanTravailSection::all();
      $elements = [];
      $plan_elements = unserialize($plans[0]->element_id);


      foreach ($plan_elements as $plan) {
        $valeur = PlanTravailElement::where('id', $plan)->get();

        array_push($elements, $valeur);
      }


      return view('semaines.create', compact(['sections', 'elements', 'plans', 'no_semaine', 'id_du_participant']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if (SemainePlan::where('no_semaine', $request->input('no_semaine'))->where('participant_id', $request->input('id_du_participant'))->exists()) {
        flash::error("Vous avez déjà complété cette semaine");

        return redirect('/');
      }

        $semaine = new SemainePlan;
        $input = Input::except('_token');
        $elements = [];
        $autresElements = [];

        $total = 0;
        // $valeur = [];
        //dd($input);
        foreach ($input as $key => $value) {

          $unElement = PlanTravailElement::find($key);
          // $unElement

          //dd($unElement->section_id);
          if ($unElement['section_id'] == 2) {
            $total += $value;

            $elements[$key] = $value;
          }
          else {
            $autresElements[$key] = 0;
          }

        }

        //dd($elements);
        $total = $total - ($request->input('no_semaine')+$request->input('id_du_participant'));
        if ($total > 100) {

          return redirect('/');
        }

        array_pop($autresElements);
        array_pop($autresElements);

        $semaine->no_semaine = $request->input('no_semaine');
        $semaine->complete = 1;

        $semaine->participant_id = $request->input('id_du_participant');
        $semaine->save();

        //dd($elements);
        if ($semaine->save()) {
          foreach ($elements as $key => $value) {
            if ($value > 0) {
              $semaine->plantravailelements()->attach($key, ['pourcentage' => $value]);
            }

          }
          foreach ($autresElements as $key => $value) {
            $semaine->plantravailelements()->attach($key, ['pourcentage' => 0]);
          }
        }


        flash::success("Semaine complétée");

        return redirect('/plan_travails/'.$request->input('id_du_participant'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
