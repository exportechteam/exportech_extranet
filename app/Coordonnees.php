<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordonnees extends Model
{
  public function Employes()
  {
    return $this->hasMany('App\Employe');
  }

  public function Participants()
  {
    return $this->hasMany('App\Participant');
  }
}
