<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanTravailSection extends Model
{
  protected $fillable = [
    'nom_section'
];

public function Elements()
  {
    return $this->hasMany('App\PlanTravailElement');
  }
}
