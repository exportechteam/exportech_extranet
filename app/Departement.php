<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
  protected $fillable = [
      'nom_departement','nombre_semaine_integration','max_semaine_prolongation'
  ];

  public function Employes()
  {
   return $this->hasMany('App\Employe');
  }

  public function Elements()
  {
   return $this->hasMany('App\PlanTravailElement');
  }
}
