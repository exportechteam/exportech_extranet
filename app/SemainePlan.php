<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemainePlan extends Model
{
    public function PlanTravailElements(){
    return $this->belongsToMany('App\PlanTravailElement', 'plan_travail_element_semaine_plan',  'semaine_plan_id', 'element_id')
    ->withPivot('pourcentage')
    ->withTimestamps();
    }
}
