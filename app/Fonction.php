<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fonction extends Model
{
  protected $fillable = [
      'nom_fonction'
  ];

  public function Employes()
  {
   return $this->hasMany('App\Employe');
  }
}
