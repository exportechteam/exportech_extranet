<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanTravailElement extends Model
{
  protected $fillable = [
    'departement_id', 'section_id', 'nom_elements'
];

  public function Departement()
  {
    return $this->belongsTo('App\Departement');
  }

  public function Section()
  {
    return $this->belongsTo('App\PlanTravailSection');
  }

  public function SemainePlans()
  {
    return $this->belongsToMany('App\SemainePlan', 'plan_travail_element_semaine_plan', 'element_id', 'semaine_plan_id')
    ->withPivot('pourcentage')
    ->withTimestamps();
  }
}
